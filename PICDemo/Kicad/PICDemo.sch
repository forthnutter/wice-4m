EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:PICDemo
LIBS:PICDemo-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "PIC DEMO to Parallel"
Date "21 jul 2017"
Rev "1"
Comp "Forthnutter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L DB25 J1
U 1 1 596E211C
P 9850 4400
F 0 "J1" H 9900 5750 70  0000 C CNN
F 1 "DB25" H 9800 3050 70  0000 C CNN
F 2 "" H 9850 4400 60  0000 C CNN
F 3 "" H 9850 4400 60  0000 C CNN
	1    9850 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 3950 2000 3950
Wire Wire Line
	2000 3950 2000 2700
Wire Wire Line
	2000 2700 7450 2700
Wire Wire Line
	7450 2700 7450 5500
Wire Wire Line
	7450 5500 9400 5500
Wire Wire Line
	7500 2600 1900 2600
Wire Wire Line
	1900 2600 1900 4050
Wire Wire Line
	1900 4050 2950 4050
Wire Wire Line
	9400 3400 7550 3400
Wire Wire Line
	7550 3400 7550 2500
Wire Wire Line
	7550 2500 1800 2500
Wire Wire Line
	1800 2500 1800 4150
Wire Wire Line
	1800 4150 2950 4150
Wire Wire Line
	9400 3600 7600 3600
Wire Wire Line
	7600 3600 7600 2400
Wire Wire Line
	7600 2400 1700 2400
Wire Wire Line
	1700 2400 1700 4250
Wire Wire Line
	1700 4250 2950 4250
Wire Wire Line
	7650 3800 9400 3800
Wire Wire Line
	7650 2300 7650 3800
Wire Wire Line
	7650 2300 1600 2300
Wire Wire Line
	1600 2300 1600 4350
Wire Wire Line
	1600 4350 2950 4350
Wire Wire Line
	9400 5600 7400 5600
Wire Wire Line
	7400 5600 7400 2800
Wire Wire Line
	7400 2800 2100 2800
Wire Wire Line
	2100 2800 2100 4450
Wire Wire Line
	2100 4450 2950 4450
Wire Wire Line
	9400 4700 7050 4700
Wire Wire Line
	7050 4700 7050 4450
Wire Wire Line
	7050 4450 4250 4450
Wire Wire Line
	7500 3200 9400 3200
Wire Wire Line
	7500 2600 7500 3200
$Comp
L PICDEM-28 J6
U 1 1 5970B353
P 3600 3900
F 0 "J6" H 3550 4750 70  0000 C CNN
F 1 "PICDEM-28" H 3550 3250 70  0000 C CNN
F 2 "~" H 3550 3900 60  0000 C CNN
F 3 "~" H 3550 3900 60  0000 C CNN
	1    3600 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 5300 7300 5300
Wire Wire Line
	7300 5300 7300 4150
Wire Wire Line
	7300 4150 4250 4150
Wire Wire Line
	9400 5100 7250 5100
Wire Wire Line
	7250 5100 7250 4250
Wire Wire Line
	7250 4250 4250 4250
$Comp
L PICDEMO-12 J7
U 1 1 5970BAD8
P 3650 5950
F 0 "J7" H 3650 6800 70  0000 C CNN
F 1 "PICDEMO-12" H 3650 6100 70  0000 C CNN
F 2 "~" H 3650 5950 60  0000 C CNN
F 3 "~" H 3650 5950 60  0000 C CNN
	1    3650 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 5400 7100 5400
Wire Wire Line
	7100 5400 7100 5200
Wire Wire Line
	9400 5200 7150 5200
Wire Wire Line
	7150 5200 7150 5000
Wire Wire Line
	7150 5000 1900 5000
Wire Wire Line
	1900 5000 1900 5200
Wire Wire Line
	1900 5200 3050 5200
Wire Wire Line
	9400 5000 7200 5000
Wire Wire Line
	7200 5000 7200 5300
Wire Wire Line
	7200 5300 4250 5300
Wire Wire Line
	9400 4800 1800 4800
Wire Wire Line
	1800 4800 1800 5300
Wire Wire Line
	1800 5300 3050 5300
Wire Wire Line
	9400 4600 7000 4600
Wire Wire Line
	7000 4600 7000 5400
Wire Wire Line
	7000 5400 4250 5400
Wire Wire Line
	9400 4400 6950 4400
Wire Wire Line
	6950 4400 6950 4700
Wire Wire Line
	6950 4700 1700 4700
Wire Wire Line
	1700 4700 1700 5400
Wire Wire Line
	1700 5400 3050 5400
Wire Wire Line
	9400 4200 6900 4200
Wire Wire Line
	6900 4200 6900 5500
Wire Wire Line
	6900 5500 4250 5500
Text Label 8850 5600 0    60   ~ 0
STROBE
Text Label 8850 5500 0    60   ~ 0
LFEED
Text Label 8850 5400 0    60   ~ 0
D0
Text Label 8850 5300 0    60   ~ 0
ERROR
Text Label 8850 5200 0    60   ~ 0
D1
Text Label 8850 5100 0    60   ~ 0
INIT
Text Label 8850 5000 0    60   ~ 0
D2
Text Label 8850 4800 0    60   ~ 0
D3
Text Label 8850 4700 0    60   ~ 0
GND
Text Label 8850 4600 0    60   ~ 0
D4
Text Label 8850 4400 0    60   ~ 0
D5
Text Label 8850 4200 0    60   ~ 0
D6
Text Label 9000 3800 0    60   ~ 0
ACK
Text Label 9000 3600 0    60   ~ 0
BUSY
Text Label 8900 3400 0    60   ~ 0
PAPER-END
Text Label 9000 3200 0    60   ~ 0
SELECT
Wire Wire Line
	3050 5500 1600 5500
Wire Wire Line
	1600 5500 1600 4600
Wire Wire Line
	1600 4600 6850 4600
Wire Wire Line
	6850 4600 6850 4000
Wire Wire Line
	6850 4000 9400 4000
Text Label 8850 4000 0    60   ~ 0
D7
Wire Wire Line
	4250 5600 7050 5600
Wire Wire Line
	7050 5600 7050 4900
Wire Wire Line
	7050 4900 9400 4900
Text Label 8850 4900 0    60   ~ 0
SELECTIN
Wire Wire Line
	3050 5700 1900 5700
Wire Wire Line
	1900 5700 1900 5850
Wire Wire Line
	1900 5850 7350 5850
Wire Wire Line
	7350 5850 7350 4500
Wire Wire Line
	7350 4500 9400 4500
Text Label 8850 4500 0    60   ~ 0
GND
Text Label 2500 3950 0    60   ~ 0
LFEED
Text Label 2500 4050 0    60   ~ 0
SELECT
Text Label 2300 4150 0    60   ~ 0
PAPER-END
Text Label 2500 4250 0    60   ~ 0
BUSY
Text Label 2500 4350 0    60   ~ 0
ACK
Text Label 2500 4450 0    60   ~ 0
STROBE
Text Label 4350 4450 0    60   ~ 0
GND
Text Label 2500 5500 0    60   ~ 0
D7
Text Label 2500 5400 0    60   ~ 0
D5
Text Label 2500 5300 0    60   ~ 0
D3
Text Label 2500 5200 0    60   ~ 0
D1
Text Label 2500 5700 0    60   ~ 0
GND
Text Label 4350 5600 0    60   ~ 0
SELECTIN
Text Label 4350 5500 0    60   ~ 0
D6
Text Label 4350 5400 0    60   ~ 0
D4
Text Label 4350 5300 0    60   ~ 0
D2
Wire Wire Line
	7100 5200 4250 5200
Text Label 4350 5200 0    60   ~ 0
D0
Text Label 4350 4250 0    60   ~ 0
INIT
Text Label 4350 4150 0    60   ~ 0
ERROR
$EndSCHEMATC
