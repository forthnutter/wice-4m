;	Minimal inline PIC Forth version 1.0 - http://sciencezero.4hv.org/computing.htm
;
;	variable fstackp = xxx		;fstackp points to the full descending stack.
;	variable ftemp1 = xxx		;ftemp1 points to the first temporary variable.
;	variable ftemp2 = xxx		;ftemp2 points to the second temporary variable.
;	include <pforth10.inc> 		;If temporary variables are not defined they will be allocated from the top of the stack.
;	finit				;Initialize Forth before use.
;
;	fstackp can be in any bank.
;	ftemp1 and ftemp2 must be valid RAM in all banks.
;
;	The stack pointer points to the last slot written to and grows from high to low memory addresses..
;	W is used as a temporary variable and the content should be assumed to be destroyed.
;	A stack diagram ( a b -- a b ) shows the content of the stack before and after the command.	 
;	The rightmost element in the stack diagram has the lowest address.



;************************************************
;	Misc
;************************************************

	;Automatically set the stack pointer for common models
	ifndef fstackp
	  ifdef __16C84
	    variable fstackp = 0x2F
	    messg "Stack pointer set to address 0x2F"
	  endif

	  ifdef __16F870
	    variable fstackp = 0x7F
	    messg "Stack pointer set to address 0x7F"
	  endif

	  ifdef __16F871
	    variable fstackp = 0x7F
	    messg "Stack pointer set to address 0x7F"
	  endif
	endif


	ifndef fstackp
	  error "Stack pointer has not been set."
	  messg "Set stack pointer by using 'variable fstackp = xxx' before 'include <pforth10.inc>'"
	  variable fstackp = 0
	  variable ftemp1 = 0
	  variable ftemp2 = 0
	else
	  if high fstackp == 0 
	    ifndef ftemp1
	      variable ftemp1 = fstackp
	    endif
	    ifndef ftemp2
	      variable ftemp2 = fstackp-1
	    endif
	  else
	    ifndef ftemp1
	      error "Temporary variable ftemp1 has not been defined."
	      messg "Use 'variable ftemp1 = xxx' before 'include <pforth10.inc>'"
	      variable ftemp1=0
	    endif
	    ifndef ftemp2
	      error "Temporary variable ftemp2 has not been defined."
	      messg "Use 'variable ftemp2 = xxx' before 'include <pforth10.inc>'"
	      variable ftemp2=0
	    endif   
	  endif

	  if (high ftemp1 != 0) || (high ftemp2 != 0)
	    error "Temporary variables ftemp1 and ftemp2 must be in page 0."
	  endif
	endif


;	Initialize and set stack pointer
finit	macro

	if (high fstackp) & 1
	  bsf	STATUS,7
	else
	  bcf	STATUS,7
	endif

	if (ftemp1 == fstackp)
	  movlw	(low fstackp)-1	;reserve room for ftemp1 and ftemp2
	else
	  movlw	(low fstackp)+1	;stack empty so point above the first available slot
	endif

	movwf	FSR
	skp			;
fnop4	return
	endm


;	Load constant		( a -- a n )
ldc	macro	n
	decf	FSR
	if n==0
	  CLRF 	INDF
	else
	  movlw	n
	  movwf	INDF
	endif
	endm


;	Push W on stack		( a -- a w )
pushw	macro
	decf	FSR
	movwf	INDF
	endm


;	Pull W on from stack	(a b -- a )
pullw	macro
	movfw	INDF
	incf	FSR
	endm

;	Skip following instruction	( a -- a )
skp	macro
	goto	$+2
	endm



;************************************************
;	Timing
;************************************************

pause	macro	n	;Pause n cpu cycles
	if n & 1
	  nop
	endif
	if n & 2
	  nop2
        endif

	if n<16
	  if n & 4
	    nop4
          endif
	  if n & 8
	    nop4
	    nop4
          endif
	else
	  if n<1025
	    movlw	  (n>>2)-1
fpaul1	    addlw	  -.1
	    skpnc
	    goto	  fpaul1
	  else
	    error "Valid range is 0-1024."
	  endif
	endif
	endm


nop2	macro
	goto	$+1
	endm


nop3	macro
	goto	$+1
	nop
	endm

nop4	macro
	call	fnop4
	endm



;************************************************
;	Arithmetic
;************************************************

;	Increase by 1		( a -- a+1 )
inc	macro
	incf	INDF
	endm


;	Decrease by 1		( a -- a-1 )
dec	macro
	decf	INDF
	endm


;	Add			( a b -- a+b )
add	macro
 	movfw	INDF
 	incf	FSR
 	addwf	INDF
	endm


;	Subtract		( a b -- a-b )
sub	macro
 	movfw	INDF
 	incf	FSR
 	subwf	INDF
	endm


;	Multiply		( a b -- low a*b high a*b )
mul	macro
	movfw	INDF		;73 cycles constant
	movwf	ftemp1
	clrf 	ftemp2	;prodH
	incf	FSR
	movfw 	INDF	;mulcnd
	clrf	INDF	;movlw 	.128
	bsf	INDF,7	;movwf 	prodL
mulsl 	rrf 	ftemp1	;mulplr
	skpnc
	addwf 	ftemp2	;prodH
	rrf 	ftemp2	;prodH
	rrf 	INDF	;prodL
	skpc
	goto 	mulsl
	decf	FSR
	movfw	ftemp2
	movwf	INDF
	endm


;	Two to the power of a 	( a -- 1<<a )
twoexp	macro
	movfw	INDF		;10 cycles constant
	movwf	ftemp1
	movlw	1
	btfsc	ftemp1,1
	movlw	4
	movwf	INDF
	btfsc	ftemp1,0
	addwf	INDF
	btfsc	ftemp1,2
	swapf	INDF
	endm



;************************************************
;	Bitwise Logical operations
;************************************************

;	AND			( a b -- a AND b ) 
and	macro
	movfw	INDF
	incf	FSR
	andwf	INDF
	endm


;	OR			( a b -- a OR b )
or	macro
	movfw	INDF
	incf	FSR
	iorwf	INDF
	endm


;	Exclusive-OR		( a b -- a EOR b )
eor	macro
	movfw	INDF
	incf	FSR
	xorwf	INDF
	endm


;	NOT AND			(a b -- a NAND b )
nand	macro
	movfw	INDF
	incf	FSR
	andwf	INDF
	comf	INDF
	endm


;	NOT OR			(a b -- a NOR b )
nor	macro
	movfw	INDF
	incf	FSR
	iorwf	INDF
	comf	INDF
	endm


;	NOT Exclusive-OR	( a b -- a NEOR b )
neor	macro
	movfw	INDF
	incf	FSR
	xorwf	INDF
	comf	INDF
	endm


;	Bit clear		( a b -- a AND NOT b )
bic	macro
	comf	INDF,W
	incf	FSR
	andwf	INDF
	endm


;	Invert			( a -- NOT a )
not	macro
	comf	INDF
	endm



;************************************************
;	Logical operations
;************************************************

;	Test if zero		( a -- a )
tst	macro
	movf	INDF
	endm


;	Boolean flag 		( a -- If a=0 then 0 else 255 )
flag	macro
	movfw	INDF
	skpz
	comf	INDF,W
	xorwf	INDF
	endm



;************************************************
;	Bit manipulation
;************************************************

;	Bit set			( a b -- a OR (1<<b) )
bset	macro	n
	twoexp
	movfw	INDF
	incf	INDF
	iorwf	INDF
	endm


;	Bit clear		( a b -- a AND NOT (1<<b) )
bclr	macro	n
	twoexp
	comf	INDF,W
	incf	INDF
	andwf	INDF
	endm


;	Bit change		( a b -- a EOR (1<<b) )
bcng	macro	n
	twoexp
	movwf	INDF
	incf	INDF
	xorwf	INDF
	endm


;	Bit test		( a b -- a )
btst	macro	n
	twoexp
	movwf	INDF
	incf	INDF
	andwf	INDF,W
	endm


;	Bit set	constant	( a -- a OR (1<<n) )
bsetc	macro	n
	bsf	INDF,n	
	endm


;	Bit clear constant	( a -- a AND NOT (1<<n) )
bclrc	macro	n
	bcf	INDF,n	
	endm


;	Bit change constant	( a -- a EOR (1<<n) )
bcngc	macro	n
	clrf	ftemp1
	bsf	ftemp1,n
	movfw	ftemp1
	xorwf	INDF
	endm


;	Bit test constant	( a -- a )
btstc	macro	n
	clrz
	btfsc	INDF,n	
	setz
	endm



;************************************************
;	Shifting
;************************************************

;	Logical shift left		( a b -- a<<b )
lsl	macro
	movfw	INDF
	incf	FSR
	comf	INDF
	skp
lsll1	rlf	INDF
	addlw	-.1
	skpnc
	goto	lsll1
	comf	INDF
	endm


;	Logical shift right		( a b -- a>>b )
lsr	macro
	movfw	INDF
	incf	FSR
	comf	INDF
	skp
lsrl1	rrf	INDF
	addlw	-.1
	skpnc
	goto	lsrl1
	comf	INDF
	endm


;	Rotate left		( a b -- a ROL b )
rol	macro
	incf	INDF,W
	movwf	ftemp1
	incf	FSR
	goto	roll2
roll1	rlf 	INDF,W
 	rlf 	INDF
roll2	decfsz	ftemp1
	goto	roll1
	endm


;	Rotate right		( a b -- a ROR b )
ror	macro
	incf	INDF,W
	movwf	ftemp1
	incf	FSR
	goto	rorl2
rorl1	rrf 	INDF,W
 	rrf 	INDF
rorl2	decfsz	ftemp1
	goto	rorl1
	endm


;	Arithmetic shift right	( a b -- a ASR b )
asr	macro
	incf	INDF,W
	movwf	ftemp1
	incf	FSR
	goto	asrl2
asrl1 	rlf 	INDF,W
 	rrf 	INDF
asrl2	decfsz	ftemp1
	goto	asrl1
	endm


;	Logical shift left by one	( a -- a<<1 )
lsl1	macro
	clrc
 	rlf	INDF
	endm


;	Logical shift left by four	( a -- a<<4 )
lsl4	macro
	swapf	INDF,W
	andlw	0xF0
	movwf	INDF
	endm


;	Logical shift right by one	( a -- a>>1 )
lsr1	macro
	clrc
 	rrf	INDF
	endm


;	Logical shift right by four	( a -- a>>4 )
lsr4	macro
	swapf	INDF,W
	andlw	0x0F
	movwf	INDF
	endm


;	Rotate left by one		( a -- a ROL 1 )
rol1	macro
 	rlf 	INDF,W
 	rlf 	INDF
	endm


;	Rotate right by one		( a -- a ROR 1 )
ror1	macro
 	rrf 	INDF
	endm


;	Rotate right by four		( a -- a ROR 4 )
ror4	macro
 	swapf 	INDF
	endm


;	Rotate left through carry by one( a -- a ROLC 1 )
rol1c	macro
 	rlf 	INDF
	endm


;	Rotate right through carry by one( a -- a RORC 1 )
ror1c	macro
 	rrf 	INDF
	endm
 

;	Arithmetic shift right by one	( a -- a / 2 )
asr1	macro
 	rlf 	INDF,W
 	rrf 	INDF
	endm



;************************************************
;	Stack manipulation
;************************************************

;	( a b -- a b b )
dup	macro
 	movfw	INDF
 	decf	FSR
 	movwf	INDF
	endm


;	( a b -- a )
drop	macro
 	incf	FSR
	endm


;	( a b -- a b a )
over	macro
	incf	FSR
 	movfw	INDF
 	decf	FSR
 	decf	FSR
 	movwf	INDF
	endm


;	( a b -- b a )
swap	macro
 	movfw	INDF
 	incf	FSR
 	xorwf	INDF,W
 	xorwf	INDF
 	decf	FSR
 	xorwf	INDF
	endm


;	( a b c -- b c a )
rot	macro
	incf	FSR
	swap
	decf	FSR
	swap	
	endm


;	( a b -- b )
nip	macro
	movfw	INDF
	incf	FSR
	movwf	INDF
	endm


;	( a b -- b a b )
tuck	macro
	swap
	over
	endm



;************************************************
;	PIC hardware
;************************************************

;	Read EEPROM		( a -- eeprom[a] )
readEE	macro
	movfw	INDF
	banksel	EEADR
	movwf	EEADR
	banksel	EECON1
	bsf	EECON1,RD
	banksel	EEDATA
	movfw	EEDATA
	movwf	INDF
	banksel 0
	endm


;	Write EEPROM		( a b -- )
writeEE	macro
	movfw	INDF
	banksel	EEADR
	movwf	EEARD
	incf	FSR
	movfw	INDF
	movwf	EEDATA
	incf	FSR
	banksel	INTCON
	clrf	INTCON		;Disable all interrupts
	banksel	IEECO1
	bsf	EECON1,WREN
	movlw	0x55
	movwf	EECON2
	movlw	0xAA
	movwf	EECON2
	bsf	EECON1,WR
waitEE	btfsc	EECON1,WR	;Wait for EEPROM write to complete (10ms typical)
	goto	waitEE
	banksel	0
	endm
