
    #ifndef PARALLEL_INC
    #define PARALLEL_INC
    
    extern par_init
    extern par_sel_latch
    extern par_led_on
    extern par_led_off
    extern par_pclk_h
    extern par_pclk_l
    extern par_sclk_h
    extern par_sclk_l
    extern par_lfeed_data
    extern par_lfeed_rfsh
    extern par_lfd7_h
    extern par_lfd7_l
    extern par_strobe
    extern par_init_t
    extern par_read_nibble
    
    #endif
    


