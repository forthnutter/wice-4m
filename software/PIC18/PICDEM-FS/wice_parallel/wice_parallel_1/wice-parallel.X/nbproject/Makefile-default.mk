#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=start.asm picforth.asm parallel.asm wice.asm

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/start.o ${OBJECTDIR}/picforth.o ${OBJECTDIR}/parallel.o ${OBJECTDIR}/wice.o
POSSIBLE_DEPFILES=${OBJECTDIR}/start.o.d ${OBJECTDIR}/picforth.o.d ${OBJECTDIR}/parallel.o.d ${OBJECTDIR}/wice.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/start.o ${OBJECTDIR}/picforth.o ${OBJECTDIR}/parallel.o ${OBJECTDIR}/wice.o

# Source Files
SOURCEFILES=start.asm picforth.asm parallel.asm wice.asm


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18f4550
MP_LINKER_DEBUG_OPTION=-r=ROM@0x7D30:0x7FFF -r=RAM@GPR:0x3EF:0x3FF
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/start.o: start.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/start.o.d 
	@${RM} ${OBJECTDIR}/start.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/start.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/start.lst\\\" -e\\\"${OBJECTDIR}/start.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/start.o\\\" \\\"start.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/start.o"
	@${FIXDEPS} "${OBJECTDIR}/start.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/picforth.o: picforth.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/picforth.o.d 
	@${RM} ${OBJECTDIR}/picforth.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/picforth.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/picforth.lst\\\" -e\\\"${OBJECTDIR}/picforth.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/picforth.o\\\" \\\"picforth.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/picforth.o"
	@${FIXDEPS} "${OBJECTDIR}/picforth.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/parallel.o: parallel.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/parallel.o.d 
	@${RM} ${OBJECTDIR}/parallel.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/parallel.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/parallel.lst\\\" -e\\\"${OBJECTDIR}/parallel.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/parallel.o\\\" \\\"parallel.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/parallel.o"
	@${FIXDEPS} "${OBJECTDIR}/parallel.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/wice.o: wice.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/wice.o.d 
	@${RM} ${OBJECTDIR}/wice.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/wice.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/wice.lst\\\" -e\\\"${OBJECTDIR}/wice.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/wice.o\\\" \\\"wice.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/wice.o"
	@${FIXDEPS} "${OBJECTDIR}/wice.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
else
${OBJECTDIR}/start.o: start.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/start.o.d 
	@${RM} ${OBJECTDIR}/start.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/start.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/start.lst\\\" -e\\\"${OBJECTDIR}/start.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/start.o\\\" \\\"start.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/start.o"
	@${FIXDEPS} "${OBJECTDIR}/start.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/picforth.o: picforth.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/picforth.o.d 
	@${RM} ${OBJECTDIR}/picforth.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/picforth.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/picforth.lst\\\" -e\\\"${OBJECTDIR}/picforth.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/picforth.o\\\" \\\"picforth.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/picforth.o"
	@${FIXDEPS} "${OBJECTDIR}/picforth.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/parallel.o: parallel.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/parallel.o.d 
	@${RM} ${OBJECTDIR}/parallel.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/parallel.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/parallel.lst\\\" -e\\\"${OBJECTDIR}/parallel.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/parallel.o\\\" \\\"parallel.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/parallel.o"
	@${FIXDEPS} "${OBJECTDIR}/parallel.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/wice.o: wice.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/wice.o.d 
	@${RM} ${OBJECTDIR}/wice.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/wice.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/wice.lst\\\" -e\\\"${OBJECTDIR}/wice.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/wice.o\\\" \\\"wice.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/wice.o"
	@${FIXDEPS} "${OBJECTDIR}/wice.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    18f4550_g.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "18f4550_g.lkr"  -p$(MP_PROCESSOR_OPTION)  -w -x -u_DEBUG -z__ICD2RAM=1 -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" -u_EXTENDEDMODE  -z__MPLAB_BUILD=1  -z__MPLAB_DEBUG=1 -z__MPLAB_DEBUGGER_ICD3=1 $(MP_LINKER_DEBUG_OPTION) -odist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
else
dist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   18f4550_g.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "18f4550_g.lkr"  -p$(MP_PROCESSOR_OPTION)  -w  -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" -u_EXTENDEDMODE  -z__MPLAB_BUILD=1  -odist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
