
    
    
    
       
	LIST P=18F4550, F=INHX32	;directive to define processor
	
	include <p18f4550.inc>
	include "picforth.inc"
	include "parallel.inc"

	global	wice_read
	global	wice_init
	
	UDATA
;
	
slatch0	    res	1	; strobe latch 0 shadow Memory


L1_CROE	    equ 0	; BIT0 = CROE
;
L1_L_NIB    equ	6	; BIT6 = Lower nibble	  
L1_H_NIB    equ	7	; BIT7 = Higher Nibble enable
slatch1	    res	1	; strobe latch 1 shadow memory
wtemp	    res 1
	    
; BIT0 = 
; BIT1 =
; BIT2 =
; BIT3 =
ARESET	    equ 4	; BIT4 =
; BIT5 =
XRAMSEL0    equ	6	; BIT6 =
XRAMSEL1    equ	7	; BIT7 =	
slatch2	    res	1	; strobe latch 2 shadow memory	
	
	
	CODE
	
;************************************************
;Description: Activate strobe
;		This done by sending data to
;		the LFEED port
; ( -- )
;************************************************
wice_strobe_on:
    PSHL    3			; sel no latch
    call    par_sel_latch	;
    call    par_led_on		; put led on
    call    par_sclk_h		; latch in d7 to turn strobe
    call    par_lfd7_h
    call    par_pclk_h		; set pclk high
    call    par_lfeed_rfsh	; update the LFEED latch
    call    par_sclk_l
    call    par_lfeed_rfsh
    return
    
;******************************************************
;Description: deactivate strobe
; ( -- )
;******************************************************
wice_strobe_off:
    call    par_sclk_l
    call    par_lfd7_h
    call    par_lfeed_rfsh
    call    par_sclk_h
    call    par_led_off
    call    par_lfeed_rfsh
    return
    
;****************************************************
; Description: Reset the address countter
; ( -- )
;****************************************************
wice_reset_counter:
    bsf	    slatch2,ARESET
    PUSHF   slatch2
    PSHL    2
    call    par_strobe
    bcf	    slatch2,ARESET
    PUSHF   slatch2
    PSHL    2
    call    par_strobe
    return
    
;*************************************************
; Decription: increment the counter
; ( g h l -- )
; g upper
; h high byte address
; l low byte adress
;**************************************************
wice_address:
    call    pf_tst3
    POPW
    bz	    wa_dec
    call    pf_dec3
    call    par_init_t
    goto    wice_address
wa_dec:
    DROP
    DROP
    DROP
    return

;********************************************
; Description: read 8 bits
; Parameters:
; Returns:  one byte of data on data stack
; ( -- d )
;*********************************************
wice_rbyte:
    PUSHF   slatch1
    BCLRL   L1_H_NIB
    CPYF    slatch1
    PSHL    1
    call    par_strobe
    call    par_read_nibble
    call    pf_lsl4

    PUSHF   slatch1
    BSETL   L1_H_NIB
    CPYF    slatch1
    PSHL    1
    call    par_strobe

    PUSHF   slatch1
    BCLRL   L1_L_NIB
    CPYF    slatch1
    PSHL    1
    call    par_strobe
    call    par_read_nibble
    OR
    PUSHF   slatch1
    BSETL   L1_L_NIB
    CPYF    slatch1
    PSHL    1
    call    par_strobe
    return

;********************************************
;Description: go read data at address
; ( g h l -- d )
; g upper address for address 16 and 17
; h high address byte address 8 to 15
; l lower addres byte address 0 to 7
; d the returning data byte
;********************************************
wice_read:
    call    wice_strobe_on
    call    wice_reset_counter
    call    wice_address
    PUSHF   slatch1
    BCLRL   L1_CROE
    CPYF    slatch1
    PSHL    1
    call    par_strobe

    call    wice_rbyte

    PUSHF   slatch1
    BSETL   L1_CROE
    CPYF    slatch1
    PSHL    1
    call    par_strobe

    call    wice_strobe_off
    return
	
	
;************************************************
; Desciption: Initilise stuff
;************************************************
wice_init:
    setf    slatch0
    PSHL    0xc7
    POPF    slatch1
    PSHL    0x11
    POPF    slatch2
;    bcf	    slatch2,XRAMSEL0
;    bcf	    slatch2,XRAMSEL1
    call    wice_strobe_on
    PUSHF   slatch2
    PSHL    2
    call    par_strobe
    PUSHF   slatch0
    PSHL    0
    call    par_strobe
    PUSHF   slatch1
    PSHL    1
    call    par_strobe
    call    wice_strobe_off
    return
    


    END