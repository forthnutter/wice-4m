;******************************************
; Parallel port interface
;
; RD0 = D0
; RD1 = D1
; RD2 = D2
; RD3 = D3
; RD4 = D4
; RD5 = D5
; RD6 = D6
; RD7 = D7
; 
; RB0 = STROBE
; RB1 = ACK
; RB2 = BUSY
; RB3 = POUT
; RB4 = SEL
; RB5 = LFEED is main control
;
; RC1 = ERR   
; RC2 = INIT
;
; RE0 = SELPTR
;
;***************************************************************************
    
	LIST P=18F4550, F=INHX32	;directive to define processor
	
	include <p18f4550.inc>
	include "picforth.inc"


STROBE	EQU RB0		; Output
ACK	EQU RB1		; Input
BUSY	EQU RB2		; Input
POUT	EQU RB3		; Input
SEL	EQU RB4		; Input
LFEED	EQU RB5		; Output
	
ERR	EQU RC1		; Input
INIT	EQU RC2		; Output
	
SELPTR	EQU RE0		; Output
	
; LF Latch bits
LA0	EQU 0
LA1	EQU 1
SCLK	EQU 2
GLED	EQU 3
UN4	EQU 4
UN5	EQU 5
PCLK	EQU 6
UN7	EQU 7
	
	
	GLOBAL par_init
	GLOBAL par_sel_latch
	GLOBAL par_led_on, par_led_off
	GLOBAL par_pclk_h, par_pclk_l
	GLOBAL par_sclk_h, par_sclk_l
	GLOBAL par_lfeed_data
	GLOBAL par_lfd7_h, par_lfd7_l
	global	par_lfeed_rfsh
	global	par_strobe
	global	par_init_t
	global par_read_nibble
	UDATA
;*****************************************************
; shadow memory for LFEED Latch
; D0 = LA0	Latch Address 0
; D1 = LA1	Latch Address 1
; D2 = ML	Rising edge to control memory buffers
; D3 = GLED	H = on L = OFF
; 0b00000011
lfshadow    res	1	; line feed latch shadow

	    
	CODE

; LFEED data
; ( d -- )
par_lfeed_data:
    POPF    LATD	    ; put data on bus
    bcf	    LATB,LFEED	    ; push that data on to the output of U30
    bsf	    LATB,LFEED	    ; we are finished
    return

; update shadow to lfeed latch
; ( -- )
par_lfeed_rfsh:
    PUSHF   lfshadow		; push shadow to stack 
    call    par_lfeed_data	; send it to LF Latch
    return
    
; select latch output
; ( d -- )
par_sel_latch:
    PSHL    B'00000011'		; mask every thing else on stack
    AND				; keep only bit 0 and 1
    PUSHF   lfshadow		; now clear the bit in shadow
    PSHL    B'11111100'		; get the mask
    AND				; get rid of bits 0 and 1
    OR				; or the incoming data with 
    POPF    lfshadow		; save result in shadow ram
    return
    
; turn on the green LED
par_led_on:
    PUSHF   lfshadow
    BSETL   GLED
    POPF    lfshadow
;    call    par_lfeed_rfsh
    return

; Turn LED off
par_led_off:
    PUSHF   lfshadow
    BCLRL   GLED
    POPF    lfshadow
;    call    par_lfeed_rfsh
    return
    
par_pclk_h:
    PUSHF   lfshadow
    BSETL   PCLK
    POPF    lfshadow
;    call    par_lfeed_rfsh
    return
    
par_pclk_l:
    PUSHF   lfshadow
    BCLRL   PCLK
    POPF    lfshadow
;    call    par_lfeed_rfsh
    return

par_sclk_h:
    PUSHF   lfshadow
    BSETL   SCLK
    POPF    lfshadow
;    call    par_lfeed_rfsh
    return
    
par_sclk_l:
    PUSHF   lfshadow
    BCLRL   SCLK
    POPF    lfshadow
;    call    par_lfeed_rfsh
    return   
    
; toggle sclk
par_sclk_t:
    call    par_sclk_l
    call    par_sclk_h
    call    par_sclk_l
    return
    
par_lfd7_h:
    PUSHF   lfshadow
    BSETL   UN7
    POPF    lfshadow
;    call    par_lfeed_rfsh
    return
    
par_lfd7_l:
    PUSHF   lfshadow
    BCLRL   UN7
    POPF    lfshadow
;    call    par_lfeed_rfsh
    return   
    
    
; send data on to latch and strobe
; ( d -- )
par_strobe_data:
    POPF    LATD		; present data
    bcf	    LATB,STROBE		; strobe low
    bsf	    LATB,STROBE		; strobe high
    bcf	    LATB,STROBE		; strobe low
    return

; Read in the nibble from
; SEL POUT BUSY and ACK
; ( -- d )
par_read_nibble:
    PUSHF   PORTB
    PSHL    0x1e
    AND
    PSHL    0
    SWAP
    POPW
    btfsc   WREG,ACK
    BSETL   3
    btfsc   WREG,BUSY
    BSETL   2
    btfsc   WREG,POUT
    BSETL   1
    btfsc   WREG,SEL
    BSETL   0
    return
    
; strobe data to an address
; ( d s -- )
; s address of strobe latch
; d data to be stored on the latch
par_strobe:
    call    par_sel_latch
    call    par_lfeed_rfsh
    call    par_strobe_data
    return

;*********************************************
; Description: init pin hi
;*********************************************
par_init_t:
    bcf	    LATC,INIT
    bsf	    LATC,INIT
    bcf	    LATC,INIT
    return
; 
; initalise the ports for use as parallel port	
par_init:
    clrf    PORTD		; set up the Port D for the data
    clrf    LATD
    movlw   0xff
    movwf   TRISD
    
    bsf	    LATB,STROBE		; init the strobe on port B
    bcf	    TRISB,STROBE
    bsf	    LATB,LFEED		; init the LFEED on port b
    bcf	    TRISB,LFEED
    
    bsf	    TRISB,ACK		; ACK is input
    bsf	    TRISB,BUSY		; BUSY is input
    bsf	    TRISB,POUT		; POUT 
    bsf	    TRISB,SEL		; SEL 
    
    bsf	    TRISC,ERR		; ERR is input port C
    
    bsf	    LATC,INIT		; init is out on port c
    bcf	    TRISC,INIT
    
    bcf	    LATE,SELPTR		; SELPTR is out on port e
    bcf	    TRISE,SELPTR
    
    movlw   0
    movwf   TRISD		; Make port D output
    
    PSHL    B'01000111'		; put default on stack
    POPF    lfshadow		; make LF shadow have the default
    
    call    par_lfeed_rfsh	; make latch refect the shadow
    return
    
    END