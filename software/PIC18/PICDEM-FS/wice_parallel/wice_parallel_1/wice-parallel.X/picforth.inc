   #IFNDEF PICFORTH_INC
   #DEFINE PICFORTH_INC

; Copy the top of stack to wreg
CPYW	macro
	movf	INDF2,W
	endm
	
   ;	Push W on stack		( W -- )
PUSHW	macro
	movwf	PREINC2
	endm
   
; POP data off stack	    ( -- W )
POPW	macro
	movf	POSTDEC2,W
	endm
	
; push literl on to stack   ( l -- )
PSHL	macro	n   
	movlw	n
	movwf	PREINC2
	endm

; copy the top of stack to file
CPYF	macro	n
	CPYW
	movwf	n
	endm
	
; push memory into stack ( f -- )
PUSHF	macro	n
	movf	n,W
	PUSHW
	endm
	
; pop stack to memory ( -- f )
POPF	macro	n
	POPW
	movwf	n
	endm
	
; inc top of data stack    ( -- )
INC	macro
	incf	INDF2
	endm

; dec top of stack value ( a -- a )
DEC	macro
	decf	INDF2
	endm
	
; add the two value on the stack ( a b -- c )
ADD	macro
	popw
	addwf	INDF2
	endm
	
; sub two values on the data stack ( a b -- c )
SUB	macro
	popw
	subwf	INDF2
	endm
	
; mul two values on the stack ( a b -- c d )
MUL	macro
	popw
	mulwf	INDF2
	popw
	movf	PRODH,W
	pushw
	movf	PRODL,W
	pushw
	endm
	
; AND			( a b -- c ) 
AND	macro
	POPW
	andwf	INDF2
	endm

;	OR			( a b -- a OR b )
OR	macro
	POPW
	iorwf	INDF2
	endm


	
	
;************************************************
;	Bit manipulation
;************************************************

;	Bit set literal		( a -- c )
BSETL	macro n
	bsf	INDF2,n
	endm	
; bit clear literal		n ( a -- c )
BCLRL	macro n
	bcf	INDF2,n
	endm
	
;************************************************
;	Shifting
;************************************************

;	Logical shift left		( a -- a<< )
LSL	macro
	rlncf	INDF2
	bcf	INDF2,0
	endm


;************************************************
;	Stack manipulation
;************************************************

;	( a b -- a b b )
DUP	macro
 	movss	[0],[1]
 	addfsr	FSR2,1
	endm


;	( a b -- a )
DROP	macro
 	subfsr	FSR2,1
	endm


;	( a b -- a b a )
OVER	macro
	incf	FSR
 	movfw	INDF
 	decf	FSR
 	decf	FSR
 	movwf	INDF
	endm


;	( a b -- b a )
SWAP	macro
 	subfsr	FSR2,1
	movss	[0],[2]
	movss	[1],[0]
	movss	[2],[1]
	addfsr	FSR2,1
	endm


;	( a b c -- b c a )
ROT	macro 
	subfsr	FSR2,2
	movss	[0],[3]
	movss	[1],[0]
	movss	[2],[1]
	movss	[3],[2]
	addfsr	FSR2,2
	endm


;	( a b -- b )
NIP	macro
	movfw	INDF
	incf	FSR
	movwf	INDF
	endm


;	( a b -- b a b )
TUCK	macro
	SWAP
	OVER
	endm
	

;************************************************
;	Logical operations
;************************************************

;	Test if zero		( a -- a )
TST	macro
	movf	INDF2
	endm	
	
	
#IFNDEF PF
   extern pf_init
   extern pf_tst3
   extern pf_dec3
   extern pf_lsl4
   
#ENDIF
   #ENDIF


