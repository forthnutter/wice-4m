	LIST P=18F4550, F=INHX32	;directive to define processor
	
#define PF
	include <p18f4550.inc>
	include "picforth.inc"
	
	global	pf_init
	global	pf_tst3
	global	pf_dec3
	global	pf_lsl4
	
	UDATA
	
pfstack	RES 16
pfsend	RES 1
pftemp	RES 1


	code
	
;	Two to the power of a 	( a -- 1<<a )
twoexp:
	PSHL	1
;	movwf	ftemp1
	movlw	1
;	btfsc	ftemp1,1
	movlw	4
;	movwf	INDF
;	btfsc	ftemp1,0
;	addwf	INDF
;	btfsc	ftemp1,2
;	swapf	INDF
	return

tst2:
    TST
    bnz	    tst2n
    SWAP
    
tst2n:
    return
    
; test three bytes on stack
; ( g h l -- g h l ? )
pf_tst3:
    movlw   0
    ROT		; get the highest
    TST		; test to see if zero
    bz	    tst31
    bsf	    WREG,0
tst31:
    ROT		; get high byte
    TST		; test for zero
    bz	    tst32
    bsf	    WREG,1
tst32:
    ROT		; get lowest
    TST
    bz	    tst33
    bsf	    WREG,2
tst33:
    PUSHW
    return

    
; Decrement Three Bytes on stack
; ( g h l -- ghl+1 )
pf_dec3:
    TST
    bnz	    dec31
    SWAP
    TST
    SWAP
    bnz	    dec32
    ROT
    TST
    ROT
    ROT
    bnz	    dec33
    return
    
dec33:
    ROT
    DEC
    ROT
    ROT
    
dec32:
    SWAP
    DEC
    SWAP

dec31:
    DEC
    return
    

;*************************************
;Description: shift left 4
;Parameter: data on stack to be shifted
;Return:    the new shifted data
; ( d -- e=d<<4 )
;**************************************
pf_lsl4:
    LSL
    LSL
    LSL
    LSL
    return




;	Initialize and set stack pointer
pf_init:
	lfsr	FSR2,pfstack-1
fnop4	return
	
	
	end