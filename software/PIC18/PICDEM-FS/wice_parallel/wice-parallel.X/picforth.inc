   #IFNDEF PICFORTH_INC
   #DEFINE PICFORTH_INC

	noexpand
; Copy the top of stack to wreg
CPYW	macro
	movf	INDF2,W
	endm
	
; copy the top of stack to file
CPYF	macro	n
	movff	INDF2,n
	endm	
	
   ;	Push W on stack		( W -- )
PUSHW	macro
	movwf	PREINC2
	endm
   
; POP data off stack	    ( -- W )
POPW	macro
	movf	POSTDEC2,W
	endm
	
; push literl on to stack   ( l -- )
PSHL	macro	n   
	movlw	n
	movwf	PREINC2
	endm
	
; push memory into stack ( f -- )
PUSHF	macro	n
	movff	n,PREINC2
	endm
	
; pop stack to memory ( -- f )
POPF	macro	n
	movff	POSTDEC2,n
	endm
	
; inc top of data stack    ( -- )
INC	macro
	incf	INDF2
	endm

; dec top of stack value ( a -- a )
DEC	macro
	decf	INDF2
	endm
	
; add the two value on the stack ( a b -- c )
ADD	macro
	POPW
	addwf	INDF2
	endm
	
; sub two values on the data stack ( a b -- c )
SUB	macro
	POPW
	subwf	INDF2
	endm
	
; mul two values on the stack ( a b -- c d )
MUL	macro
	POPW
	mulwf	INDF2
	POPW
	movf	PRODH,W
	PUSHW
	movf	PRODL,W
	PUSHW
	endm
	
; AND			( a b -- c ) 
AND	macro
	POPW
	andwf	INDF2
	endm

;	OR			( a b -- a OR b )
OR	macro
	POPW
	iorwf	INDF2
	endm

;   Basic clear
CLR	macro
	clrf	PREINC2
	endm
	
;************************************************
;	Bit manipulation
;************************************************

;	Bit set literal		( a -- c )
BSETL	macro n
	bsf	INDF2,n
	endm	
; bit clear literal		n ( a -- c )
BCLRL	macro n
	bcf	INDF2,n
	endm
	
;************************************************
;	Shifting
;************************************************

;	Logical shift left		( a -- a<< )
LSL	macro
	rlncf	INDF2
	bcf	INDF2,0
	endm

; Logical shift right
; ( a -- a>> )
LSR	macro
	rrncf	INDF2
	bcf	INDF2,7
	endm
	

;************************************************
;	Stack manipulation
;************************************************

;	( a -- a a )
DUPW	macro
	CPYW
	PUSHW
	endm

DUPOLD	macro
 	movss	[0],[1]
 	addfsr	FSR2,1
	endm
	
DUP	macro
	PUSHF	INDF2
	endm

;	( a b -- a b a b )
DUP2	macro
	movlw	-1
	PUSHF	PLUSW2
	PUSHF	PLUSW2
	endm
	

;	( a b -- a )
DROP	macro
 	subfsr	FSR2,1
	endm


;	( a b -- a b a )
OVER	macro
	subfsr	FSR2,1
 	movss	[0],[2]
 	addfsr	FSR2,1
	movss	[0],[2]
 	addfsr	FSR2,1
	endm


;	( a b -- b a )
SWAP	macro
 	subfsr	FSR2,1
	movss	[0],[2]
	movss	[1],[0]
	movss	[2],[1]
	addfsr	FSR2,1
	endm

;	( a b c d -- c d a b )
SWAP2	macro
	subfsr	FSR2,3
	movss	[0],[4]
	movss	[1],[5]
	movss	[2],[0]
	movss	[3],[1]
	movss	[4],[2]
	movss	[5],[3]
	addfsr	FSR2,3
	endm
	

;	( a b c -- b c a )
ROT	macro 
	subfsr	FSR2,2
	movss	[2],[3]
	movss	[1],[2]
	movss	[0],[1]
	movss	[3],[0]
	addfsr	FSR2,2
	endm
	
;	( a b c -- c a b )
ROTA	macro
	subfsr FSR2,2
	movss	[0],[3]
	movss	[1],[0]
	movss	[2],[1]
	movss	[3],[2]
	addfsr	FSR2,2
	endm
	
;	( a b -- b )
NIP	macro
	subfsr	FSR2,1
	movss	[1],[0]
	endm


;	( a b -- b a b )
TUCK	macro
	SWAP
	OVER
	endm
	

;************************************************
;	Logical operations
;************************************************

;	Test if zero	( a -- a ) z=?
TST	macro
	movf	INDF2
	endm	

;   ( a b -- a b )  z=?
TST2	macro
	DUP2
	OR
	TST
	DROP
	endm

;********************************************
; PIC18 specific 
; move flash label into to stack
; basically move flash address to stack
;
	
; ( name -- u h l )
PUSHFA	macro n
	PSHL UPPER n
	PSHL HIGH n
	PSHL LOW n
	endm

;Push File address to stack
; ( name -- h l )
PUSHRA	macro	n
	PSHL HIGH n
	PSHL LOW n
	endm
	
;Pop 16 bit data into the file address
POPRA	macro	n
	POPF	n+1	    ;w has lobyte
	POPF	n
;	movwf	n,0
	endm

;get address from pointer
PTR	macro
        POPF    FSR0L
        POPF    FSR0H
        movlw   0
        PUSHF   PLUSW0
        movlw   1
        PUSHF   PLUSW0
	endm	
	
	
	
#IFNDEF PF
   extern pf_init
   extern pf_tst3
   extern pf_dec3
   extern pf_lsl4
   extern compare, compare2
   extern flash_move, file_move
   extern push_ptr
   extern store
   
#ENDIF
   #ENDIF


