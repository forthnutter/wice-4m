
    
       
	LIST P=18F4550, F=INHX32	;directive to define processor

	include <p18f4550.inc>
	include "picforth.inc"
	include "parallel.inc"
	include "linefeed.inc"
	include "strobe.inc"
	include	"serial.inc"
	include "list.inc"
	include	"memory.inc"
	include "sme.inc"
	include	"interp.inc"



	global	rline_init



RDL_START   equ	    0	    ;The start state
RDL_READ    equ	    1	    ;read serial into pad
RDL_SWAP    equ	    2	    ;swap the double buffer


CR	    equ	    .13
LF	    equ	    .10
SP	    equ	    .32

    cblock 0
	STATE
	MPH		;holds pointer to memory structure
	MPL
	PADH		;general pointer to pad memory
	PADL
	INDEX
	RDL_SIZE
    endc

	UDATA


rline_sme:	res SME_SIZE
rline_param:	res RDL_SIZE




	CODE


;**********************************************************
;turn a 8bit binary number into two byte nibbles
;( n -- nh nl )
;**********************************************************
nibbles:
    DUP			    ; copy the byte
    PSHL    0x0f	    ; push mask bit
    AND			    ; Top four bits cleared
    SWAP		    ; get new byte
    LSR
    LSR
    LSR
    LSR			    ;move the upper nubble to bottom nibble
    SWAP		    ;return nibble 
    return

;***********************************************************
;turn the binary nibble into ascii nibble
;( n -- h )
;***********************************************************
ascii_nibble:
    DUP			    ;copy because we are testing
    PSHL    .10		    ;test if < than 10
    call    compare
    bn	    ascii_30	    ;if < 10 do 30+
    DROP
    PSHL    0x37	    ;push magic number
    ADD			    ;add them and we have hex
    return

ascii_30:
    DROP
    PSHL    0x30	    ;push magic number
    ADD			    ;add them and we have hex
    return


;**********************************************************
;Turn a 8bit binary number into two byte hex number
; ( n -- a b )
;**********************************************************
string_hex:
    call    nibbles	    ; convert one byte into two nibbles
    call    ascii_nibble    ; conver nible to ascii hex
    SWAP		    ;now the high nibble
    call    ascii_nibble    ;conver nibble to ascii hex
    SWAP
    return


;*********************************************************
;set the state of the parameter structure
; ( state ph pl -- )
;*********************************************************
rline_set_state:
    POPF    FSR0L
    POPF    FSR0H
    movlw   STATE
    POPF    PLUSW0
    return

;**********************************************************
;State 01: Test data on stack to see if it is CR LF SP
; ( dd -- dd/00 ) Z
;***********************************************************
rline_test:
    DUP				;make a copy
    PSHL    CR			;Carige Return or enter
    call    compare		; A == B
    DROP			;don't whats on stack
    bz	    rline_clr

    DUP				; make another copy
    PSHL    LF			;line feed
    call    compare		;A == B
    DROP
    bz	    rline_clr

    DUP
    PSHL    SP			;space final frontier
    call    compare		;A == B
    DROP
    bz	    rline_clr

    return

rline_clr:
    DROP
    CLR
    return


;**************************************************************
;State 00: Initlise pad functions
;**************************************************************
rl_s00:
    POPF    FSR0L		;make sure FSR0 has Parameter
    POPF    FSR0H		;
    call    memory_open		;get a lump memory
    bz	    rl_s00_00
    movlw   MPL
    POPF    PLUSW0		;pop low address into param
    movlw   MPH
    POPF    PLUSW0		;pop high address to param

    movlw   MPH
    PUSHF   PLUSW0
    movlw   MPL
    PUSHF   PLUSW0
    call    memory_owner	;get the location of pad memory

    movlw   PADL
    POPF    PLUSW0
    movlw   PADH
    POPF    PLUSW0

    CLR				;clear stack
    movlw   INDEX
    POPF    PLUSW0		;clear index 

    PSHL    RDL_READ
    PUSHF   FSR0H
    PUSHF   FSR0L
    call    rline_set_state
    return

rl_s00_00:
    DROP
    DROP
    return



;*************************************************************
;State 01: read one byte from serial port and store into pad
;	    test to see data is <CR> or enter key
;**************************************************************
rl_s01:
    POPF    FSR0L		; make sure we have fsr 0
    POPF    FSR0H

    call    serial_get		;one byte serial
    bz	    rl_s01_end		;Z flag indicates no data
    DUP				;copy value and echo back
    call    serial_put		;echo it back
    DUP				;make another copy for testing
    call    rline_test		;test for cr lf and sp

    nop
    movlw   PADH		;here we make FSR1 Pad pointer
    movff   PLUSW0,FSR1H	;pointer to pad
    movlw   PADL
    movff   PLUSW0,FSR1L	;point current pad

    movlw   INDEX		;get index
    movf    PLUSW0,w		;index value in w
    POPF    PLUSW1		;save serial data here

    movlw   INDEX		;now increment index
    incf    PLUSW0

    ;CR is the end of line
    PSHL    CR			;test for carage return
    call    compare
    bz	    rl_s01_cr
    DROP			;lose the compare byte
    return

rl_s01_cr:
    DROP			;drop cr and start up another state
    PSHL    RDL_SWAP		;the swap state
    PUSHF   FSR0H
    PUSHF   FSR0L
    call    rline_set_state	;new state
    return

rl_s01_end:
    DROP			; remove whatever is on stack
    return


;***********************************************
;state 2: send the memory to interpiter
; ( h l -- )
;************************************************
rl_s02:
    POPF    FSR0L		;make sure we have parameters in FSR0
    POPF    FSR0H

    movlw   MPH			;Memory struct
    PUSHF   PLUSW0		;
    movlw   MPL
    PUSHF   PLUSW0		;
    call    interp_put

    CLR				; index = 0
    movlw   INDEX		;set index to begining
    POPF    PLUSW0

    PSHL    RDL_START		; state = READ
    PUSHF   FSR0H
    PUSHF   FSR0L
    call    rline_set_state

    return



;**********************************************************
;Display the state of Readline task
;( state -- )
;*********************************************************
rline_print_state:
    PUSHFA  readline_state	; display name of program
    call    serial_fprint
    call    string_hex		;convert 8bit binary to ascii hex
    SWAP
    call    serial_put
    call    serial_put
    call    serial_crlf
    return


;************************************************************
;Read Line task loop by colecting serial data
; ( ph pl -- ) 
; parameter structure is on the stack
;************************************************************
rline_task:
    POPF    FSR0L		;FSR0 = Parameters
    POPF    FSR0H		;FSR0 is now pointer to structure
    PUSHF   FSR0H		;now move it back to stack
    PUSHF   FSR0L		;

    movlw   STATE		;if (state == START)
    PUSHF   PLUSW0		;state is on the stack
 
;    DUP				;we duplicate so we can print
;    call    rline_print_state	;send the state data

    PSHL    RDL_START
    call    compare
    DROP
    btfsc   STATUS,Z
    goto    rl_s00		;then

    movlw   STATE		;if (state == READ)
    PUSHF   PLUSW0
    PSHL    RDL_READ
    call    compare
    DROP
    btfsc   STATUS,Z
    goto    rl_s01		;then

    movlw   STATE		;if (state == SWAP)
    PUSHF   PLUSW0
    PSHL    RDL_SWAP
    call    compare
    DROP
    bz	    rl_s02		;then

    DROP			;we don't need whats on stack
    DROP
    return



;************************************************************
;Initilise things for startup
; Ch Cl -- h l
;***********************************************************
rline_init:
    PSHL    RDL_START		;state = START
    PUSHRA  rline_param
    call    rline_set_state

    PUSHFA  rline_task		;put task into sme structure
    PUSHRA  rline_sme
    call    sme_add_task
    
    PUSHRA  rline_param
    PUSHRA  rline_sme
    call    sme_add_param	;put parameter structure into task structure

    PUSHRA  rline_sme
    call    sme_task_add	;now load the rline stucture into sme

    return



readline_state:
    data    "Read-Line State:",0

	end
