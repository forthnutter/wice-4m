
    #ifndef SME_INC
    #define SME_INC
    
    extern	sme_init
    extern	sme_task
    extern	sme_add_task
    extern	sme_add_param
    extern	sme_task_add
    
    cblock  0
	LST_NEXT_H, LST_NEXT_L
	LST_PREV_H, LST_PREV_L
	SME_PARAM_H, PARAM_L
	SME_OWNER_U, SME_OWNER_H, SME_OWNER_L
	SME_SIZE
    endc
    
    #endif

