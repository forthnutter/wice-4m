    
    
       
	LIST P=18F4550, F=INHX32	;directive to define processor
	
	include <p18f4550.inc>
	include "picforth.inc"
	include "parallel.inc"
	include "linefeed.inc"
	include "strobe.inc"

	global	serial_init, serial_tx_isr, serial_rx_isr
	global	serial_put, serial_get, serial_cr, serial_lf
	global	serial_crlf, serial_lfcr
	global	serial_fprint, serial_rprint

STX_SIZE:	equ	8
SRX_SIZE:	equ	8
	
	UDATA
stx_start:	RES	1
stx_buff:	RES	STX_SIZE
stx_end:	RES	1
stx_cnt:	RES	1

srx_start:	RES	1
srx_buff:	RES	SRX_SIZE
srx_end:	RES	1
srx_cnt:	RES	1

;in the interrupt we need to save FSR
fsr_save_h:	RES	1
fsr_save_l:	RES	1

;Here we save FSR out side of interrupt
fsrh_save:	RES	1
fsrl_save:	RES	1
	
	CODE


;***********************************************
;Test if start and end are the same
;***********************************************
stx_same:
    BANKSEL stx_start
    movf    stx_start,w
    cpfseq  stx_end
    bra	    ststx
    bsf	    STATUS,C
    return
ststx:
    bcf	    STATUS,C
    return

    
;***************************************************
;Transmmit data
;Warnning do not use forth macros here
;***************************************************
serial_tx_isr:
    BANKSEL stx_cnt		; make sure we are in thright bank
    btfss   TXSTA,TRMT		; test to see if ready to send another
    bra	    tx_ntrdy		; leave if not ready
    tstfsz  stx_cnt		;see if have somthing in buffer
    bra	    tx_send		;send data if same
    bcf	    PIE1,TXIE		;turn off inerrupt 
    bcf	    PIR1,TXIF		;not needed now
tx_ntrdy:
    return

tx_send:
    movff   FSR0H,fsr_save_h
    movff   FSR0L,fsr_save_l

    lfsr    FSR0,stx_buff
    movf    stx_start,w
    movff   PLUSW0,TXREG
    incf    stx_start		;increment index to next byte
    decf    stx_cnt		;counter decremented
    movlw   STX_SIZE
    cpfseq  stx_start
    bra	    tx_end
    clrf    stx_start

tx_end:
    movff   fsr_save_h,FSR0H
    movff   fsr_save_l,FSR0L
    return

;************************************************
;Serial input data comes here
;************************************************
serial_rx_isr:
    btfss   RCSTA,CREN
    bra	    rx_ntrdy
    btfsc   RCSTA,FERR
    bra	    rx_error
    btfsc   RCSTA,OERR
    bra	    rx_error

rx_read:
    movff   FSR0H,fsr_save_h	    ; save the fsr
    movff   FSR0L,fsr_save_l
    lfsr    FSR0,srx_buff
    movf    srx_end,w
    movff   RCREG,PLUSW0
    incf    srx_end
    incf    srx_cnt

    movlw   SRX_SIZE
    cpfseq  srx_end
    bra	    rx_end
    clrf    srx_end
rx_end:
    movff   fsr_save_h,FSR0H	    ;restore fsr
    movff   fsr_save_l,FSR0L
    return

rx_error:
    bcf	    RCSTA,CREN
    nop
    bsf	    RCSTA,CREN
    movf    RCREG,w
rx_ntrdy:
    return

;**************************************************
;Send byte to serial port
; ( dd -- ) carry flag set end ptr reset
;	    z flag set data was not sent
; this routine uses forth macros
;*************************************************
serial_put:
    BANKSEL stx_cnt
    movlw   STX_SIZE
    cpfseq  stx_cnt
    bra	    sp_good
    bsf	    STATUS,Z
    bsf	    STATUS,C
    bsf	    PIE1,TXIE
    DROP
    return

sp_good:
;    bcf	    INTCON,GIE		;disable all interupts
    movff   FSR0H,fsrh_save		;save fsr
    movff   FSR0L,fsrl_save
    lfsr    FSR0,stx_buff
    movf    stx_end,w
    POPF    PLUSW0
    incf    stx_end
    incf    stx_cnt
    movlw   STX_SIZE
    cpfseq  stx_end
    bra	    serial_put_carry
    clrf    stx_end
    bcf	    STATUS,C
    bcf	    STATUS,Z
    bsf	    PIE1,TXIE
    bra	    serial_put_end
serial_put_carry:
    bsf	    PIE1,TXIE
    bsf	    STATUS,C
    bcf	    STATUS,Z
serial_put_end:
;    bsf	    INTCON,GIE		;enable all interupts
    movff   fsrh_save,FSR0H		;Restore fsr
    movff   fsrl_save,FSR0L
    return


;******************************************************
;Serial Send out Carige return
;*****************************************************
serial_cr:
    PSHL    0x0d		;push cr
    call    serial_put		;now print it
    return


;******************************************************
;Serial send out Line Feed Character
;******************************************************
serial_lf:
    PSHL    0x0a	    ;Push LF
    call    serial_put
    return

;*********************************************************
;Serial send out CR and LF
;******************************************************
serial_crlf:
    call    serial_cr
    call    serial_lf
    return

;********************************************************
;Serial send out LF and CR
;********************************************************
serial_lfcr:
    call    serial_lf
    call    serial_cr
    return


;*******************************************************
;Serial get value from the que
;( -- dd ) Z = empty stack unknown
;	   z = data valid
;*******************************************************
serial_get:
;    bcf	    INTCON,GIE		; disable interupts
    tstfsz  srx_cnt
    bra	    sg_read
;    bsf	    INTCON,GIE
    PSHL    0
    bsf	    STATUS,Z
    return

sg_read:
    movff   FSR0H,fsrh_save		;save the fsr because
    movff   FSR0L,fsrl_save		;so other routine do not destroy
    lfsr    FSR0,srx_buff		;fsr now point serial receive buffer
    movf    srx_start,w
    PUSHF   PLUSW0
    incf    srx_start
    decf    srx_cnt
    movlw   SRX_SIZE
    cpfseq  srx_start
    bra	    sg_end2
sg_end:
    clrf    srx_start
sg_end2:
;    bsf	    INTCON,GIE
    bcf	    STATUS,Z
    movff   fsrh_save,FSR0H		; restore the original fsr
    movff   fsrl_save,FSR0L		; 
    return


;*****************************************************
; Serial print from flash
; ( u h l -- )
;*****************************************************
serial_fprint:
    POPF    TBLPTRL	    ; load able ptr
    POPF    TBLPTRH
    POPF    TBLPTRU

sf_read:
    TBLRD*+		    ; get the data and increment
sf_loop:
    PUSHF   TABLAT	    ; save data on stack
    TST			    ; test data on stack
    bz	    sf_end	    ; if data is zero the leave
    call    serial_put	    ; send data to serial port
    bz	    sf_loop	    ; if data didn't go loop
    bra	    sf_read

sf_end:
    DROP		    ; after tst data is still on stack
    return

;**************************************************************
; Serial print from file ( RAM )
; ( h l -- )
;**************************************************************
serial_rprint:
sr_next:
    POPF    FSR0L
    POPF    FSR0H
    PUSHF   POSTINC0
    PUSHF   FSR0H
    SWAP
    PUSHF   FSR0L
    SWAP
sr_loop:
    TST
    bz	    sr_end
    DUP
    call    serial_put
    bz	    sr_loop
    DROP
    bra	    sr_next

sr_end:
    DROP
    DROP
    DROP
    return


;********************************************************
; Init the serial port
;*******************************************************
serial_init:
    BANKSEL stx_start

    clrf    stx_start
    clrf    stx_end
    clrf    stx_cnt
    clrf    srx_start
    clrf    srx_end
    clrf    srx_cnt
    
    bsf	    TRISC,RX
    bcf	    TRISC,TX

    bsf	    TXSTA,TXEN
    bcf	    TXSTA,CSRC
    bcf	    TXSTA,TX9

    bcf	    TXSTA,SENDB
    bsf	    TXSTA,BRGH
    bsf	    TXSTA,TX9D
    bcf	    TRISC,TX
    bsf	    TRISC,RX

    bcf	    RCSTA,SREN
    bcf	    RCSTA,ADDEN

    bcf	    BAUDCON,ABDEN
    bcf	    BAUDCON,WUE
    bsf	    BAUDCON,BRG16
    bcf	    BAUDCON,SCKP
    movlw   0xe1
    movwf   SPBRG
    movlw   0x04
    movwf   SPBRGH

    bcf	    TXSTA,SYNC
    bsf	    RCSTA,SPEN

    bsf	    PIE1,TXIE
    bsf	    PIE1,RCIE

    bcf	    RCSTA,RX9
    bsf	    RCSTA,CREN

    bsf	    INTCON,PEIE
    bsf	    INTCON,GIE

    movf    RCREG,w
    movf    RCREG,w
    movf    RCREG,w
    return

    end
