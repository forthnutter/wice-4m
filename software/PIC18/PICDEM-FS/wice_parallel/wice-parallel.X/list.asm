
;********************************************************
;an attemp to creat a linked list in pic assembly
;********************************************************

       
	LIST P=18F4550, F=INHX32	;directive to define processor
	
	include <p18f4550.inc>
	include "picforth.inc"

	global	list_additem
	global	list_removeitem
	global	list_nextitem
	global	list_init


item.l:	equ FSR0L
item.h:	equ FSR0H
head.l:	equ FSR1L
head.h:	equ FSR1H


    cblock  0
	next.h, next.l
	prev.h, prev.l
	item_size
    endc

    udata
fsr_save_l  res 1
fsr_save_h  res	1

    CODE



;*****************************************************
;Return the address of next item
;( *list -- &next )
;*****************************************************
list_nextitem:
    POPF    FSR0L		; lower part of pointer
    POPF    FSR0H		; high part of pointer
    movlw   next.h
    PUSHF   PLUSW0		; save high address of next
    movlw   next.l
    PUSHF   PLUSW0		; save low address of next
    return


;********************************************************
;add a item to item head end
;( head item -- head' )
;********************************************************
list_additem:
    movff   FSR0L,fsr_save_l	;save fsr 0
    movff   FSR0H,fsr_save_h

    POPF    item.l  ;FSR0L	;move item address into 0
    POPF    item.h  ;FSR0H
    DUP2			;dup head
    POPF    head.l  ;FSR1L	;remember head is a pointer to list
    POPF    head.h  ;FSR1H
    TST2
    bz	    list_headstart	;if head is zero means its new
    POPF    head.l  ;FSR1L	;move head address into 1
    POPF    head.h  ;FSR1H
    movlw   prev.h		;index previous
    movff   PLUSW1,PLUSW0	;item->previous = head->previous
    movlw   prev.l
    movff   PLUSW1,PLUSW0

    movlw   prev.h		;index previous
    movff   item.h,PLUSW1	;head->previous = item
    movlw   prev.l		;
    movff   item.l,PLUSW1

    PUSHF    head.h ;FSR1H	;save head on stack
    PUSHF    head.l ;FSR1L

    movlw   prev.h		;get the previous item
    movff   PLUSW0,FSR1H	;item->next = item->previous->next
    movlw   prev.l		;
    movff   PLUSW0,FSR1L
    movlw   next.h
    movff   PLUSW1,PLUSW0
    movlw   next.l
    movff   PLUSW1,PLUSW0

    movlw   prev.h		;item->previous->next = item
    movff   PLUSW0,FSR1H
    movlw   prev.l
    movff   PLUSW0,FSR1L
    movlw   next.h
    movff   FSR0H,PLUSW1
    movlw   next.l
    movff   FSR0L,PLUSW1
    goto    list_addend

list_headstart:
    DROP			;we drop what ever we have as head
    DROP
    movff   FSR0L,FSR1L		;head = item
    movff   FSR0H,FSR1H		;
    movlw   next.h		;item->next = item
    movff   FSR0H,PLUSW0	;next item of item is equal to item
    movlw   next.l
    movff   FSR0L,PLUSW0	;
    movlw   prev.h		;item->previous = item
    movff   FSR0H,PLUSW0	;prevuois item is now item
    movlw   prev.l
    movff   FSR0L,PLUSW0
    PUSHF   FSR1H
    PUSHF   FSR1L

list_addend:
    movff   fsr_save_l,FSR0L
    movff   fsr_save_h,FSR0H
    return

;********************************************************
;remove item from head list
;( head item -- head' )
;********************************************************
list_removeitem:
    movff   FSR0L,fsr_save_l
    movff   FSR0H,fsr_save_h

    POPF    FSR0L		;move item into FSR0
    POPF    FSR0H
    POPF    FSR1L		;move head into FSR1
    POPF    FSR1H
    
    PUSHF   FSR1H		;if(item == head)
    PUSHF   FSR1L
    PUSHF   FSR0H
    PUSHF   FSR0L
    call    compare2
    DROP
    bnz	    listremend

    movlw   next.h		;if(item->next == item)
    PUSHF   PLUSW0
    movlw   next.l
    PUSHF   PLUSW0
    PUSHF   FSR0H
    PUSHF   FSR0L
    call    compare2
    DROP
    bnz	    itemnext

    movlw   next.h		;if(item->next == item->previous)
    PUSHF   PLUSW0
    movlw   next.l
    PUSHF   PLUSW0
    movlw   prev.h
    PUSHF   PLUSW0
    movlw   prev.l
    PUSHF   PLUSW0
    call    compare2
    DROP
    bnz	    listremend

    clrf    FSR1H		;head = 0
    clrf    FSR1L

    movlw   next.h		;item->next = item
    movff   FSR0H,PLUSW0
    movlw   next.l
    movff   FSR0L,PLUSW0

    movlw   prev.h		;item->previous = item
    movff   FSR0H,PLUSW0
    movlw   prev.l
    movff   FSR0L,PLUSW0
    bra	    listremend

itemnext:
    movlw   next.h		;head = item->next
    movff   PLUSW0,FSR1H
    movlw   next.l
    movff   PLUSW0,FSR1L

listremend:
    PUSHF   FSR1H		;save head to stack
    PUSHF   FSR1L
    
    movlw   next.h		;item->next->previous = item->previous
    movff   PLUSW0,FSR1H	;
    movlw   next.l
    movff   PLUSW0,FSR1L
    movlw   prev.h
    movff   PLUSW0,PLUSW1
    movlw   prev.l
    movff   PLUSW0,PLUSW1

    movlw   prev.h		;item->previous->next = item->next
    movff   PLUSW0,FSR1H
    movlw   prev.l
    movff   PLUSW0,FSR1L
    movlw   next.h
    movff   PLUSW0,PLUSW1
    movlw   next.l
    movff   PLUSW0,PLUSW1

    movff   fsr_save_l,FSR0L
    movff   fsr_save_h,FSR0H
    return

;******************************************************************
; initilise list
; ( h l -- )
;******************************************************************
list_init:
    POPF    FSR0L
    POPF    FSR0H

    movlw   next.h
    movff   FSR0H,PLUSW0
    movlw   next.l
    movff   FSR0L,PLUSW0

    movlw   prev.h
    movff   FSR0H,PLUSW0
    movlw   prev.l
    movff   FSR0L,PLUSW0

    return



    END