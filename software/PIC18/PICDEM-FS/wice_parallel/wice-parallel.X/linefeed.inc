
    #ifndef LINEFEED_INC
    #define LINEFEED_INC
    
    extern lf_init
    extern lf_sel_latch
    extern lf_led_on
    extern lf_led_off
    extern lf_pclk_high
    extern lf_pclk_low
    extern lf_sclk_high, lf_sclk_low
    extern lf_data4_high, lf_data4_low
    extern lf_data7_high, lf_data7_low
    extern lf_shadow
    
    #endif
    


