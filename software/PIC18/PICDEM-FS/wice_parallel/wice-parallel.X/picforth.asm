	LIST P=18F4550, F=INHX32	;directive to define processor
	
#define PF
	include <p18f4550.inc>
	include "picforth.inc"
	
	global	pf_init
	global	pf_tst3
	global	pf_dec3
	global	pf_lsl4
	global	compare, compare2
	global	flash_move, file_move
	global	push_ptr
	global	store

	
	UDATA
	
pfstack	RES 16
pfsend	RES 1
pftemp	RES 1

fsr_h	res 1	    ;save fsr to restore later
fsr_l	res 1

	code
	
;	Two to the power of a 	( a -- 1<<a )
twoexp:
	PSHL	1
;	movwf	ftemp1
	movlw	1
;	btfsc	ftemp1,1
	movlw	4
;	movwf	INDF
;	btfsc	ftemp1,0
;	addwf	INDF
;	btfsc	ftemp1,2
;	swapf	INDF
	return

tst2:
    TST
    bnz	    tst2n
    SWAP
    
tst2n:
    return
    
; test three bytes on stack
; ( g h l -- g h l ? )
pf_tst3:
    movlw   0
    ROT		; get the highest
    TST		; test to see if zero
    bz	    tst31
    bsf	    WREG,0
tst31:
    ROT		; get high byte
    TST		; test for zero
    bz	    tst32
    bsf	    WREG,1
tst32:
    ROT		; get lowest
    TST
    bz	    tst33
    bsf	    WREG,2
tst33:
    PUSHW
    return

    
; Decrement Three Bytes on stack
; ( g h l -- ghl+1 )
pf_dec3:
    TST
    bnz	    dec31
    SWAP
    TST
    SWAP
    bnz	    dec32
    ROT
    TST
    ROT
    ROT
    bnz	    dec33
    return
    
dec33:
    ROT
    DEC
    ROT
    ROT
    
dec32:
    SWAP
    DEC
    SWAP

dec31:
    DEC
    return
    

;*************************************
;Description: shift left 4
;Parameter: data on stack to be shifted
;Return:    the new shifted data
; ( d -- e=d<<4 )
;**************************************
pf_lsl4:
    LSL
    LSL
    LSL
    LSL
    return

;*******************************************
;Compare two bytes on stack 
;return values on stack 00 equal 1 a is greater -1 a is less than
;( a b -- ?? ) z flag also set
compare:
    SUB
    TST
    bz	    compare_exit
    bn	    compare_neg
    DROP
    PSHL    1
compare_exit:
    return
compare_neg:
    DROP
    PSHL    -1
    return

;*******************************************************
;compare two 16 bit (words) on the stack
;return value 0 equal 1 is greater -1 is less
; ( a b c d -- ?? ) z flag also set
;*******************************************************
compare2:
    ROTA
    call compare
    ROT
    call compare
    OR
    TST
    bz	    compare2_exit
    bn	    compare2_neg
    DROP
    PSHL    1
compare2_exit:
    return
compare2_neg:
    DROP
    PSHL    -1
    return


;******************************************************
; move memory to memory number of bytes
; ( hd ld hs ls n -- )
file_move:
    POPW
    POPF    FSR0L
    POPF    FSR0H
    POPF    FSR1L
    POPF    FSR1H
    PUSHW
rm_loop:
    movf    POSTINC0,w
    movwf   POSTINC1
    DEC
    TST
    bnz	    rm_loop
    DROP
    clrf    INDF1
    return

;********************************************************
; move flash memory to file memory
; ( hd ld us hs ls n -- )
flash_move:
    POPW
    POPF    TBLPTRL	    ; load source tptr
    POPF    TBLPTRH
    POPF    TBLPTRU
    POPF    FSR0L
    POPF    FSR0H
    PUSHW
fm_loop:
    TBLRD*+		    ; get the data and increment
    movf    TABLAT,w
    movwf   POSTINC0
    DEC
    TST
    bnz	    fm_loop
    DROP
    clrf    INDF0
    return


;return the 16 bit value from the 16 bit address on stack
; ( mh ml -- ph pl )
push_ptr:
    movff   FSR0L,fsr_l	    ; save FSR
    movff   FSR0H,fsr_h
    PTR
;    POPF    FSR0L	    ; get new FSR off stack
;    POPF    FSR0H
;    PUSHF   POSTINC0	    ; Push data from FSR to stack
;    PUSHF   POSTINC0
    movff   fsr_h,FSR0H	    ; now restore FSR
    movff   fsr_l,FSR0L
    return

;Store byte to memory
;( db fh fl -- )
store:
    movff   FSR0L,fsr_l	    ; save FSR
    movff   FSR0H,fsr_h
    POPF    FSR0L	    ; Get the new FSR off stck
    POPF    FSR0H
    POPF    INDF0	    ; POP data off stack into FSR pointer
    movff   fsr_h,FSR0H
    movff   fsr_l,FSR0L
    return


;	Initialize and set stack pointer
pf_init:
	lfsr	FSR2,pfstack-1
fnop4	return
	
	
	end