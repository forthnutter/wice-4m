    
    
       
	LIST P=18F4550, F=INHX32	;directive to define processor

	include <p18f4550.inc>
	include "picforth.inc"
	include "parallel.inc"
	include "linefeed.inc"
	include "strobe.inc"
	include	"serial.inc"
	include	"list.inc"
	include	"sme.inc"



	global	interp_init
	global	interp_task
	global	interp_put

ITP_START   equ	    0

    cblock  0
	STATE
	MPH
	MPL
	INTP_SIZE
    endc



	UDATA
interp_sme:     res	SME_SIZE
interp_param:	res	INTP_SIZE

ip_fsrh:	res	1
ip_fsrl:	res	1

ip_wordlist:	res	2



	CODE


;*********************************************************
;Put memory adress
; ( h l -- )
;*********************************************************
interp_put:
    movff   FSR0H,ip_fsrh	;save FSR
    movff   FSR0L,ip_fsrl

    PUSHRA  ip_wordlist
    PTR
    SWAP2
    call    list_additem
    POPRA   ip_wordlist
    movff   ip_fsrl,FSR0L
    movff   ip_fsrh,FSR0H	;restore fsr
    return

;*********************************************************
;set the state of the parameter structure
; ( state ph pl -- )
;*********************************************************
interp_set_state:
    POPF    FSR0L
    POPF    FSR0H
    movlw   STATE
    POPF    PLUSW0
    return


;*****************************************************
;start state
;
;*****************************************************
it_s00:
    PUSHRA  ip_wordlist
    PTR

    DROP
    DROP
    return


;**********************************************************
;( ph pl -- )
interp_task:
    POPF    FSR0L	;put parameter into FSR0
    POPF    FSR0H
    PUSHF   FSR0H
    PUSHF   FSR0L

    movlw   STATE
    PUSHF   PLUSW0	    ; w has the state of task
    PSHL    ITP_START
    call    compare
    DROP
    bz	    it_s00

    
    
    return

;*****************************************************
; Initalise task to run
; ( Ch Cl -- h l )
;*****************************************************
interp_init:
    PSHL    ITP_START		;state = START
    PUSHRA  interp_param
    call    interp_set_state

    PUSHFA  interp_task
    PUSHRA  interp_sme
    call    sme_add_task
    PUSHRA  interp_param
    PUSHRA  interp_sme
    call    sme_add_param

    PUSHRA  interp_sme
    call    sme_task_add

    PSHL    0
    PSHL    0
    POPRA   ip_wordlist
    
    
    return



	END
