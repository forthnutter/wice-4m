#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-debug.mk)" "nbproject/Makefile-local-debug.mk"
include nbproject/Makefile-local-debug.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=debug
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=start.asm picforth.asm wice.asm linefeed.asm strobe.asm parallel.asm serial.asm interp.asm readline.asm list.asm sme.asm memory.asm

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/start.o ${OBJECTDIR}/picforth.o ${OBJECTDIR}/wice.o ${OBJECTDIR}/linefeed.o ${OBJECTDIR}/strobe.o ${OBJECTDIR}/parallel.o ${OBJECTDIR}/serial.o ${OBJECTDIR}/interp.o ${OBJECTDIR}/readline.o ${OBJECTDIR}/list.o ${OBJECTDIR}/sme.o ${OBJECTDIR}/memory.o
POSSIBLE_DEPFILES=${OBJECTDIR}/start.o.d ${OBJECTDIR}/picforth.o.d ${OBJECTDIR}/wice.o.d ${OBJECTDIR}/linefeed.o.d ${OBJECTDIR}/strobe.o.d ${OBJECTDIR}/parallel.o.d ${OBJECTDIR}/serial.o.d ${OBJECTDIR}/interp.o.d ${OBJECTDIR}/readline.o.d ${OBJECTDIR}/list.o.d ${OBJECTDIR}/sme.o.d ${OBJECTDIR}/memory.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/start.o ${OBJECTDIR}/picforth.o ${OBJECTDIR}/wice.o ${OBJECTDIR}/linefeed.o ${OBJECTDIR}/strobe.o ${OBJECTDIR}/parallel.o ${OBJECTDIR}/serial.o ${OBJECTDIR}/interp.o ${OBJECTDIR}/readline.o ${OBJECTDIR}/list.o ${OBJECTDIR}/sme.o ${OBJECTDIR}/memory.o

# Source Files
SOURCEFILES=start.asm picforth.asm wice.asm linefeed.asm strobe.asm parallel.asm serial.asm interp.asm readline.asm list.asm sme.asm memory.asm


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-debug.mk dist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18f4550
MP_LINKER_DEBUG_OPTION=-r=ROM@0x7D30:0x7FFF -r=RAM@GPR:0x3EF:0x3FF
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/start.o: start.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/start.o.d 
	@${RM} ${OBJECTDIR}/start.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/start.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/start.lst\\\" -e\\\"${OBJECTDIR}/start.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/start.o\\\" \\\"start.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/start.o"
	@${FIXDEPS} "${OBJECTDIR}/start.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/picforth.o: picforth.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/picforth.o.d 
	@${RM} ${OBJECTDIR}/picforth.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/picforth.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/picforth.lst\\\" -e\\\"${OBJECTDIR}/picforth.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/picforth.o\\\" \\\"picforth.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/picforth.o"
	@${FIXDEPS} "${OBJECTDIR}/picforth.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/wice.o: wice.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/wice.o.d 
	@${RM} ${OBJECTDIR}/wice.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/wice.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/wice.lst\\\" -e\\\"${OBJECTDIR}/wice.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/wice.o\\\" \\\"wice.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/wice.o"
	@${FIXDEPS} "${OBJECTDIR}/wice.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/linefeed.o: linefeed.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/linefeed.o.d 
	@${RM} ${OBJECTDIR}/linefeed.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/linefeed.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/linefeed.lst\\\" -e\\\"${OBJECTDIR}/linefeed.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/linefeed.o\\\" \\\"linefeed.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/linefeed.o"
	@${FIXDEPS} "${OBJECTDIR}/linefeed.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/strobe.o: strobe.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/strobe.o.d 
	@${RM} ${OBJECTDIR}/strobe.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/strobe.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/strobe.lst\\\" -e\\\"${OBJECTDIR}/strobe.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/strobe.o\\\" \\\"strobe.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/strobe.o"
	@${FIXDEPS} "${OBJECTDIR}/strobe.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/parallel.o: parallel.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/parallel.o.d 
	@${RM} ${OBJECTDIR}/parallel.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/parallel.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/parallel.lst\\\" -e\\\"${OBJECTDIR}/parallel.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/parallel.o\\\" \\\"parallel.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/parallel.o"
	@${FIXDEPS} "${OBJECTDIR}/parallel.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/serial.o: serial.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/serial.o.d 
	@${RM} ${OBJECTDIR}/serial.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/serial.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/serial.lst\\\" -e\\\"${OBJECTDIR}/serial.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/serial.o\\\" \\\"serial.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/serial.o"
	@${FIXDEPS} "${OBJECTDIR}/serial.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/interp.o: interp.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/interp.o.d 
	@${RM} ${OBJECTDIR}/interp.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/interp.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/interp.lst\\\" -e\\\"${OBJECTDIR}/interp.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/interp.o\\\" \\\"interp.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/interp.o"
	@${FIXDEPS} "${OBJECTDIR}/interp.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/readline.o: readline.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/readline.o.d 
	@${RM} ${OBJECTDIR}/readline.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/readline.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/readline.lst\\\" -e\\\"${OBJECTDIR}/readline.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/readline.o\\\" \\\"readline.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/readline.o"
	@${FIXDEPS} "${OBJECTDIR}/readline.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/list.o: list.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/list.o.d 
	@${RM} ${OBJECTDIR}/list.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/list.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/list.lst\\\" -e\\\"${OBJECTDIR}/list.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/list.o\\\" \\\"list.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/list.o"
	@${FIXDEPS} "${OBJECTDIR}/list.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/sme.o: sme.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sme.o.d 
	@${RM} ${OBJECTDIR}/sme.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/sme.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/sme.lst\\\" -e\\\"${OBJECTDIR}/sme.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/sme.o\\\" \\\"sme.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/sme.o"
	@${FIXDEPS} "${OBJECTDIR}/sme.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/memory.o: memory.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/memory.o.d 
	@${RM} ${OBJECTDIR}/memory.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/memory.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/memory.lst\\\" -e\\\"${OBJECTDIR}/memory.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/memory.o\\\" \\\"memory.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/memory.o"
	@${FIXDEPS} "${OBJECTDIR}/memory.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
else
${OBJECTDIR}/start.o: start.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/start.o.d 
	@${RM} ${OBJECTDIR}/start.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/start.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/start.lst\\\" -e\\\"${OBJECTDIR}/start.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/start.o\\\" \\\"start.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/start.o"
	@${FIXDEPS} "${OBJECTDIR}/start.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/picforth.o: picforth.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/picforth.o.d 
	@${RM} ${OBJECTDIR}/picforth.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/picforth.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/picforth.lst\\\" -e\\\"${OBJECTDIR}/picforth.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/picforth.o\\\" \\\"picforth.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/picforth.o"
	@${FIXDEPS} "${OBJECTDIR}/picforth.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/wice.o: wice.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/wice.o.d 
	@${RM} ${OBJECTDIR}/wice.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/wice.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/wice.lst\\\" -e\\\"${OBJECTDIR}/wice.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/wice.o\\\" \\\"wice.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/wice.o"
	@${FIXDEPS} "${OBJECTDIR}/wice.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/linefeed.o: linefeed.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/linefeed.o.d 
	@${RM} ${OBJECTDIR}/linefeed.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/linefeed.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/linefeed.lst\\\" -e\\\"${OBJECTDIR}/linefeed.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/linefeed.o\\\" \\\"linefeed.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/linefeed.o"
	@${FIXDEPS} "${OBJECTDIR}/linefeed.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/strobe.o: strobe.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/strobe.o.d 
	@${RM} ${OBJECTDIR}/strobe.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/strobe.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/strobe.lst\\\" -e\\\"${OBJECTDIR}/strobe.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/strobe.o\\\" \\\"strobe.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/strobe.o"
	@${FIXDEPS} "${OBJECTDIR}/strobe.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/parallel.o: parallel.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/parallel.o.d 
	@${RM} ${OBJECTDIR}/parallel.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/parallel.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/parallel.lst\\\" -e\\\"${OBJECTDIR}/parallel.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/parallel.o\\\" \\\"parallel.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/parallel.o"
	@${FIXDEPS} "${OBJECTDIR}/parallel.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/serial.o: serial.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/serial.o.d 
	@${RM} ${OBJECTDIR}/serial.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/serial.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/serial.lst\\\" -e\\\"${OBJECTDIR}/serial.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/serial.o\\\" \\\"serial.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/serial.o"
	@${FIXDEPS} "${OBJECTDIR}/serial.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/interp.o: interp.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/interp.o.d 
	@${RM} ${OBJECTDIR}/interp.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/interp.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/interp.lst\\\" -e\\\"${OBJECTDIR}/interp.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/interp.o\\\" \\\"interp.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/interp.o"
	@${FIXDEPS} "${OBJECTDIR}/interp.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/readline.o: readline.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/readline.o.d 
	@${RM} ${OBJECTDIR}/readline.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/readline.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/readline.lst\\\" -e\\\"${OBJECTDIR}/readline.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/readline.o\\\" \\\"readline.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/readline.o"
	@${FIXDEPS} "${OBJECTDIR}/readline.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/list.o: list.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/list.o.d 
	@${RM} ${OBJECTDIR}/list.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/list.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/list.lst\\\" -e\\\"${OBJECTDIR}/list.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/list.o\\\" \\\"list.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/list.o"
	@${FIXDEPS} "${OBJECTDIR}/list.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/sme.o: sme.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sme.o.d 
	@${RM} ${OBJECTDIR}/sme.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/sme.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/sme.lst\\\" -e\\\"${OBJECTDIR}/sme.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/sme.o\\\" \\\"sme.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/sme.o"
	@${FIXDEPS} "${OBJECTDIR}/sme.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/memory.o: memory.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/memory.o.d 
	@${RM} ${OBJECTDIR}/memory.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/memory.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/memory.lst\\\" -e\\\"${OBJECTDIR}/memory.err\\\" $(ASM_OPTIONS)  -y  -o\\\"${OBJECTDIR}/memory.o\\\" \\\"memory.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/memory.o"
	@${FIXDEPS} "${OBJECTDIR}/memory.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    18f4550_g.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "18f4550_g.lkr"  -p$(MP_PROCESSOR_OPTION)  -w -x -u_DEBUG -z__ICD2RAM=1 -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" -u_EXTENDEDMODE  -z__MPLAB_BUILD=1  -z__MPLAB_DEBUG=1 -z__MPLAB_DEBUGGER_ICD3=1 $(MP_LINKER_DEBUG_OPTION) -odist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
else
dist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   18f4550_g.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "18f4550_g.lkr"  -p$(MP_PROCESSOR_OPTION)  -w  -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" -u_EXTENDEDMODE  -z__MPLAB_BUILD=1  -odist/${CND_CONF}/${IMAGE_TYPE}/wice-parallel.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/debug
	${RM} -r dist/debug

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
