    #ifndef SERIAL_INC
    #define SERIAL_INC
    
    extern	serial_init, serial_tx_isr, serial_rx_isr
    extern	serial_put, serial_get, serial_cr, serial_lf
    extern	serial_crlf, serial_lfcr
    extern	serial_fprint, serial_rprint
    #endif


