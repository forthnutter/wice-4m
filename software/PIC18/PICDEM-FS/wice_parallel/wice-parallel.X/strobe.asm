;******************************************
; Strobe function only works when Line Feed has been initlised
; with wice initilise
;
; Strobe has three shadow memory latches accessed vis the LF latch
; slatch0
; slatch1
; slatch2
;
;***************************************************************************
    
	LIST P=18F4550, F=INHX32	;directive to define processor
	
	include <p18f4550.inc>
	include "picforth.inc"
	include "parallel.inc"
	include "linefeed.inc"

	GLOBAL	strobe_init
	GLOBAL	strobe
	GLOBAL	strobe_address_reset
	global	st2_ar_toggle
	global	strobe0
	global	strobe1
	global	strobe2

	UDATA


L0_PROFILE0 equ	0	; PROFILE 0 Basic ROM 2716
L0_PROFILE1 equ 1	;    "    1 Handle Address 14 to 16
L0_PROFILE2 equ	2	;    "    2 Handle CS1 CS2
L0_PROFILE3 equ	3	;    "    3

slatch0	    res	1	; strobe latch 0 shadow Memory


; bits for Strobe Latch 1
L1_CROE	    equ 0	; BIT0 = CROE RAM Output Enable
L1_EMUSEL   equ	1	; BIT1 = EMUSEL emulator select
L1_BSEL	    equ 2	; BSEL = Buffer select
L1_ENA17    equ	3	; ENA17 = Enable Address 17

L1_L_NIB    equ	6	; BIT6 = Lower nibble	  
L1_H_NIB    equ	7	; BIT7 = Higher Nibble enable

slatch1	    res	1	; strobe latch 1 shadow memory

wtemp	    res 1


; bits for Strobe latch 2	    
; BIT0 = 
; BIT1 =
; BIT2 =
; BIT3 =
ARESET	    equ 4	; BIT4 =
; BIT5 =
XRAMSEL0    equ	6	; BIT6 =
XRAMSEL1    equ	7	; BIT7 =	
slatch2	    res	1	; strobe latch 2 shadow memory	
	    
	CODE

    
    
    
; strobe data to an address
; ( d s -- )
; s address of strobe latch
; d data to be stored on the latch
strobe:
    call    lf_sel_latch
    call    lf_shadow
    call    par_strobe_data
    return

; Strobe to latch 0 with data on stck
; ( d -- )
; d is data
strobe0:
    DUP
    POPF    slatch0
    PSHL    0
    call    strobe
    return

strobe1:
    DUP
    POPF    slatch1
    PSHL    1
    call    strobe
    return


; strobe 2 signal
; set address reset pin high
; ( -- )
st2_ar_high:
    PUSHF   slatch2
    BSETL   ARESET
    POPF    slatch2
    return

; clear address reset pin low
;   ( -- )
st2_ar_low:
    PUSHF   slatch2
    BCLRL   ARESET
    POPF    slatch2
    return


    
; Put data into strobe 2 latch
;   ( d -- )
strobe2:
    DUP
    POPF    slatch2
    PSHL    2
    call    strobe
    return


; toggle address reset from low to high to low
;   ( -- )
st2_ar_toggle:
    call    st2_ar_low	    ;make sure shadow is 
    call    st2_ar_high	    ;now make it go high
    PUSHF   slatch2
    call    strobe2
    call    st2_ar_low
    PUSHF   slatch2
    call    par_strobe_data ; don't bother about setting LF Latch
    return



; ( -- )
strobe_address_reset:
    bsf	    slatch2,ARESET
    PUSHF   slatch2
    call    strobe2
    bcf	    slatch2,ARESET
    PUSHF   slatch2
    call    strobe2
    return

; 
; initalise the strobe data	
strobe_init:
    setf    slatch0
    PSHL    0xc7
    POPF    slatch1
    PSHL    0x11
    POPF    slatch2
    PUSHF   slatch2
    call    strobe2
    PUSHF   slatch0
    call    strobe0
    PUSHF   slatch1
    call    strobe1
    return
    
    END