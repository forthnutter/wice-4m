;******************************************
; Parallel port interface
;
; RD0 = D0
; RD1 = D1
; RD2 = D2
; RD3 = D3
; RD4 = D4
; RD5 = D5
; RD6 = D6
; RD7 = D7
; 
; RB0 = STROBE
; RB1 = ACK
; RB2 = BUSY
; RB3 = POUT
; RB4 = SEL
; RB5 = LFEED is main control
;
; RC1 = ERR   
; RC2 = INIT
;
; RE0 = SELPTR
;
;***************************************************************************
    
	LIST P=18F4550, F=INHX32	;directive to define processor
	
	include <p18f4550.inc>
	include "picforth.inc"


STROBE	EQU RB0		; Output
ACK	EQU RB1		; Input
BUSY	EQU RB2		; Input
POUT	EQU RB3		; Input
SEL	EQU RB4		; Input
LFEED	EQU RB5		; Output
	
ERR	EQU RC1		; Input
INIT	EQU RC2		; Output
	
SELPTR	EQU RE0		; Output
		
	
	GLOBAL	par_init
	GLOBAL	par_lf_data
	GLOBAL	par_strobe_data
	GLOBAL	par_inc_address
	GLOBAL	par_read_nibble
	global	par_selptr_data

	UDATA

	CODE

; send data out LFEED
; ( d -- )
par_lf_data:
    POPF    LATD	    ; put data on bus
    bcf	    LATB,LFEED	    ; push that data on to the output of U30
    nop
    bsf	    LATB,LFEED	    ; we are finished
    nop
    bcf	    LATB,LFEED	    ; finsih off low
    return    
    
; send data on to latch and strobe
; ( d -- )
par_strobe_data:
;    POPF    LATD		; present data
    bcf	    LATB,STROBE		; strobe low
    bsf	    LATB,STROBE		; strobe high
    POPF    LATD		; present data
    nop
    bcf	    LATB,STROBE		; strobe low
    return

; Read in the nibble from
; SEL POUT BUSY and ACK
; ( -- d )
par_read_nibble:
    PUSHF   PORTB
    PSHL    0x1e
    AND
    PSHL    0
    SWAP
    POPW
    btfsc   WREG,BUSY
    BSETL   3
    btfsc   WREG,ACK
    BSETL   2
    btfsc   WREG,POUT
    BSETL   1
    btfsc   WREG,SEL
    BSETL   0
    return
    

;*********************************************
; Description: init pin hi
;*********************************************
par_inc_address:
    bcf	    LATC,INIT
    nop
    bsf	    LATC,INIT
    nop
    bcf	    LATC,INIT
    return

;***********************************************
;Description: SELPTR pin high then low with data
;		This writes data to RAM
; ( d -- )
;***********************************************
par_selptr_data:
    bcf	    LATE,SELPTR		; make sure low
    POPF    LATD		; pop stack to port d
    nop
    bsf	    LATE,SELPTR		; selptr high
    nop
    nop
    bcf	    LATE,SELPTR		; selptr low
    return


; 
; initalise the ports for use as parallel port	
par_init:
    clrf    PORTD		; set up the Port D for the data
    clrf    LATD
    movlw   0xff
    movwf   TRISD
    
    bcf	    LATB,STROBE		; init the strobe on port B
    bcf	    TRISB,STROBE
    bcf	    LATB,LFEED		; init the LFEED on port b
    bcf	    TRISB,LFEED
    
    bsf	    TRISB,ACK		; ACK is input
    bsf	    TRISB,BUSY		; BUSY is input
    bsf	    TRISB,POUT		; POUT 
    bsf	    TRISB,SEL		; SEL 
    
    bsf	    TRISC,ERR		; ERR is input port C
    
    bcf	    LATC,INIT		; init is out on port c
    bcf	    TRISC,INIT
    
    bcf	    LATE,SELPTR		; SELPTR is out on port e
    bcf	    TRISE,SELPTR
    
    clrf    TRISD
    movlw   0
    movwf   TRISD		; Make port D output
    
    return
    
    END