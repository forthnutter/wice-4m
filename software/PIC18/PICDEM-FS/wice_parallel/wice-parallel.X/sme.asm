;*****************************************************
;Forthnutters State Machine Engine
;*****************************************************
    include <p18f4550.inc>
    include "picforth.inc"
    include "list.inc"


    global  sme_init
    global  sme_task
    global  sme_add_task
    global  sme_add_param
    global  sme_task_add

    cblock  0
	LST_NEXT_H, LST_NEXT_L
	LST_PREV_H, LST_PREV_L
	SME_PARAM_H, SME_PARAM_L
	SME_OWNER_U, SME_OWNER_H, SME_OWNER_L
	SME_SIZE
    endc

    udata
sme_temp_h      res     1
sme_temp_l	res	1

    code


;**********************************************************
; Add a task to the task list
; ( Ch Cl Th TL -- Ch Cl )
;**********************************************************
sme_task_add:
    SWAP2
    call    push_ptr
    SWAP2
    call    list_additem
    return


;**********************************************************
; Add parammeter address to structure
; ( h l h l -- )
;***********************************************************
sme_add_param:
    POPF    FSR0L	    ; get the address of structure
    POPF    FSR0H
    movlw   SME_PARAM_L
    POPF    PLUSW0
    movlw   SME_PARAM_H
    POPF    PLUSW0
    return

;***********************************************************
; Add a task to structure
; ( u h l h l -- )
;***********************************************************
sme_add_task:
    POPF    FSR0L	    ; get the address of structure
    POPF    FSR0H
    movlw   SME_OWNER_L
    POPF    PLUSW0
    movlw   SME_OWNER_H
    POPF    PLUSW0
    movlw   SME_OWNER_U
    POPF    PLUSW0
    return


;***********************************************************
; extract the executable address from list structure
; ( h l -- u h l )
sme_task_address:
    POPF    FSR0L		; FSR0 load with structure
    POPF    FSR0H
    movlw   SME_OWNER_U
    PUSHF   PLUSW0		; save owner u to stack
    movlw   SME_OWNER_H
    PUSHF   PLUSW0		; save high address to stack
    movlw   SME_OWNER_L
    PUSHF   PLUSW0		; save low addres to stack
    return

;***********************************************************
; extract the parameter addres of structure
; ( h l -- h l )
;***********************************************************
sme_task_param:
    POPF    FSR0L	    ; get the address into ram pointer
    POPF    FSR0H
    movlw   SME_PARAM_H
    PUSHF   PLUSW0	    ; save parameter high address to stack
    movlw   SME_PARAM_L
    PUSHF   PLUSW0	    ; save low parameter address to stack
    return



;*****************************************************
; execute one cycle of the state machine engine
; ( h l -- nh nl )  stack has pointer to the task list
;*****************************************************
sme_task:
    DUP2			; copy address of state machine structure
    DUP2			; copy the address
    call    sme_task_param	; get task parameter on the stack
    SWAP2			; move parameters down the stack
    call    sme_task_address	; get task address to execute
    POPW			; w has PCL
    POPF    PCLATH		; PCLATH has high byte
    POPF    PCLATU		; PCLATU has upper byte
    callw			; ( h l -- nh nl ) run task
    nop				; we come back with the structure
    call    list_nextitem	; get the address of next item
    return




;*****************************************************
; Initilise the SME
; ( Ch Cl -- )
;*****************************************************
sme_init:
    POPF    FSR0L		;
    POPF    FSR0H		; put the current sme address into FSR0
    clrf    POSTINC0		; clear 0
    clrf    POSTINC0		; clear 1
    return

    end


