;******************************************
; Line Feed routines
;
;
;***************************************************************************
    
	LIST P=18F4550, F=INHX32	;directive to define processor
	
	include <p18f4550.inc>
	include "picforth.inc"
	include "parallel.inc"


	
; LF Latch bits
LA0	EQU 0
LA1	EQU 1
SCLK	EQU 2
GLED	EQU 3
UN4	EQU 4
UN5	EQU 5
PCLK	EQU 6
UN7	EQU 7
	
	
	GLOBAL lf_init
	GLOBAL lf_sel_latch
	GLOBAL lf_led_on, lf_led_off
	GLOBAL lf_pclk_high, lf_pclk_low
	GLOBAL lf_sclk_high, lf_sclk_low
	GLOBAL lf_data4_high, lf_data4_low
	GLOBAL lf_data7_high, lf_data7_low
	GLOBAL lf_shadow

	UDATA
;*****************************************************
; shadow memory for LFEED Latch
; D0 = LA0	Latch Address 0
; D1 = LA1	Latch Address 1
; D2 = ML	Rising edge to control memory buffers
; D3 = GLED	H = on L = OFF
; 0b00000011
lfshadow    res	1	; line feed latch shadow

	    
	CODE


; select latch output
; ( d --  )
lf_sel_latch:
    PSHL    B'00000011'		; mask every thing else on stack
    AND				; keep only bit 0 and 1
    PUSHF   lfshadow		; now clear the bit in shadow
    PSHL    B'11111100'		; get the mask
    AND				; get rid of bits 0 and 1
    OR				; or the incoming data with
    POPF    lfshadow		; pop stack to shadow
    return

;*********************************************************    
;Description: turn on the green LED
;*******************************************************
lf_led_on:
    PUSHF   lfshadow		; push shadow to stack
    BSETL   GLED		; bit set led
    POPF    lfshadow		; pop stack to shadow
    return

;*********************************************************
;Description: Turn LED off
lf_led_off:
    PUSHF   lfshadow		; push shadow to stack
    BCLRL   GLED		; bit clear
    POPF    lfshadow		; pop stack to shadow
    return

;******************************************************
;Decription:	set pclk
;*******************************************************    
lf_pclk_high:
    PUSHF   lfshadow		; push shadow to stack
    BSETL   PCLK		; bit clk high
    POPF    lfshadow		; pop stcack to shadow
    return

;******************************************************
;Description:	clear pclk
;*****************************************************
lf_pclk_low:
    PUSHF   lfshadow		; push shadow to stck
    BCLRL   PCLK		; clear clk bit
    POPF    lfshadow		; pop stck to shadow
    return


;************************************************************
;Description:	set sclk
;***********************************************************
lf_sclk_high:
    PUSHF   lfshadow		; push shadow to stack
    BSETL   SCLK		; set clk bit on stack
    POPF    lfshadow		; pop stack to shadow
    return

;*************************************************
;Decsription:	clear sclk
;*****************************************************    
lf_sclk_low:
    PUSHF   lfshadow
    BCLRL   SCLK
    POPF    lfshadow
    return


;*****************************************************
;Description:	data 4 of LF shadow is high
;*****************************************************
lf_data4_high:
    PUSHF   lfshadow
    BSETL   UN4
    POPF    lfshadow
    return

;*************************************************
;Description:	data 4 of LF shadow low
;*************************************************
lf_data4_low:
    PUSHF   lfshadow
    BCLRL   UN4
    POPF    lfshadow
    return

;************************************************
;Description: data 7 of LF shadow high
;************************************************
lf_data7_high:
    PUSHF   lfshadow
    BSETL   UN7
    POPF    lfshadow
    return

;******************************************
;Description: data 7 of LF shadow low
;*******************************************    
lf_data7_low:
    PUSHF   lfshadow
    BCLRL   UN7
    POPF    lfshadow
    return   


;***********************************
;Description:	send what ever is in shadow
;		to linfeed latch
;*****************************************
lf_shadow:
    PUSHF   lfshadow		;push shadow ram to stack
    call    par_lf_data
    return

    
; initalise the ports for use as parallel port	
lf_init:
    PSHL    B'01000111'		; put default on stack
    DUP				; save into shadow and port
    POPF    lfshadow		; shadow init
    call    par_lf_data		; make latch refect the shadow
    return
    
    END