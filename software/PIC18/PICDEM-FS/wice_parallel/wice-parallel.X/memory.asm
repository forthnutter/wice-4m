;*********************************************************
; a little memory manager
;*********************************************************


    
       
	LIST P=18F4550, F=INHX32	;directive to define processor

	include <p18f4550.inc>
	include "picforth.inc"
	include "parallel.inc"
	include "linefeed.inc"
	include "strobe.inc"
	include	"serial.inc"
	include "list.inc"
	include "sme.inc"

	global	memory_init
	global	memory_open
	global	memory_owner


    cblock  0
	LST_NEXT_H, LST_NEXT_L
	LST_PREV_H, LST_PREV_L
	POOL_H, POOL_L
	POOL_SIZE
    endc

MEM_POOL_SIZE	equ	16
MEM_POOL_NUM	equ	4
MEM_POOL_NSIZE	equ	MEM_POOL_SIZE * MEM_POOL_NUM


    udata
pool_alist_h	res	1	    ; available list
pool_alist_l	res	1

pool_blist_h	res	1	    ; used list
pool_blist_l	res	1

pool_tlist_h	res	1	    ; temperary
pool_tlist_l	res	1	    ; list

pool_list0	res	POOL_SIZE
pool_list1	res	POOL_SIZE
pool_list2	res	POOL_SIZE
pool_list3	res	POOL_SIZE

mem_pool0	res	MEM_POOL_SIZE
mem_pool1	res	MEM_POOL_SIZE
mem_pool2	res	MEM_POOL_SIZE
mem_pool3	res	MEM_POOL_SIZE

fsr_h		res	1	    ;save fsr here
fsr_l		res	1

    code


;***************************************************************
;Open a memory use
; ( -- mp )
;***************************************************************
memory_open:
    PUSHRA  pool_alist_h	; Push pointer alist
    call    push_ptr		; extract address of item
    DUP2			; ( ah al -- ah al bh bl )
    DUP2			; ( ah al -- ah al bh bl ) 
    call    list_removeitem	; ( ah al bh bl -- ch cl ) Remove item
    POPRA   pool_alist_h	; save the list into pointer
    TST2			; test stack for zero
    return

;**************************************************************
;Add the address of memory loactio to the pool list
; ( mp pl -- )
;****************************************************************
memory_pool_owner:
    POPF    FSR0L
    POPF    FSR0H
    movlw   POOL_L
    POPF    PLUSW0
    movlw   POOL_H
    POPF    PLUSW0
    return


; ****************************************************************
; get the address of pool from pool structure
; ( ph pl -- poh pol )
; ***************************************************************
memory_owner:
    movff   FSR0L,fsr_l
    movff   FSR0H,fsr_h
    POPF    FSR0L
    POPF    FSR0H
    movlw   POOL_H
    PUSHF   PLUSW0
    movlw   POOL_L
    PUSHF   PLUSW0
    movff   fsr_h,FSR0H
    movff   fsr_l,FSR0L
    return

;********************************************************************
; Initilise a very small memory manager
;********************************************************************
memory_init:
    BANKSEL pool_alist_h
    clrf    pool_alist_h
    clrf    pool_alist_l
    clrf    pool_blist_h
    clrf    pool_blist_l
    clrf    pool_tlist_h
    clrf    pool_tlist_l

    PUSHRA  pool_list0
    call    list_init
    PUSHRA  pool_list1
    call    list_init
    PUSHRA  pool_list2
    call    list_init
    PUSHRA  pool_list3
    call    list_init

    PUSHRA  mem_pool0
    PUSHRA  pool_list0
    call    memory_pool_owner
    
    PUSHRA  mem_pool1
    PUSHRA  pool_list1
    call    memory_pool_owner

    PUSHRA  mem_pool2
    PUSHRA  pool_list2
    call    memory_pool_owner

    PUSHRA  mem_pool3
    PUSHRA  pool_list3
    call    memory_pool_owner

    PUSHRA  pool_alist_h
    call    push_ptr
    PUSHRA  pool_list0
    call    list_additem
    POPRA   pool_alist_h

    PUSHRA  pool_alist_h
    call    push_ptr
    PUSHRA  pool_list1
    call    list_additem
    POPRA   pool_alist_h

    PUSHRA  pool_alist_h
    call    push_ptr
    PUSHRA  pool_list2
    call    list_additem
    POPRA   pool_alist_h

    PUSHRA  pool_alist_h
    call    push_ptr
    PUSHRA  pool_list3
    call    list_additem
    POPRA   pool_alist_h


    return

    end