
    
    
    
       
	LIST P=18F4550, F=INHX32	;directive to define processor
	
	include <p18f4550.inc>
	include "picforth.inc"
	include "parallel.inc"
	include "linefeed.inc"
	include "strobe.inc"


	global	wice_read
	global	wice_write_a
	global	wice_write_b
	global	wice_init
	
	UDATA
;

	
	
	CODE
	

;****************************************************
; Description: Reset the address counter
; ( -- )
;****************************************************
wice_reset_counter:
    call    strobe_address_reset
    return
    


;**************************************************
;Description: Make Halt Signal Lo
; ( -- )
;**************************************************
wice_halt_lo:
    return

;*****************************************************
;Decription: before we start access to RAM we restore
;	    Control latch to normal.
; ( -- )
;*****************************************************
wice_normalise:
    PSHL    3		    ; select no strobes latches
    call    lf_sel_latch    ; setup the latch shadow
    call    lf_sclk_high    ;
    call    lf_led_off	    ; 47h is latched
    call    lf_pclk_high
    call    lf_data7_low
    call    lf_shadow
    return


;*****************************************************
;Description: open port A or B sets access to A or B
; ( ? -- )
;******************************************************
wice_open:
    call    lf_sclk_low
    call    lf_led_on
    call    lf_pclk_high
    call    lf_data7_high

    POPW
    bz	    wod4z
    call    lf_data4_high
    bra	    wocon
wod4z:
    call    lf_data4_low
wocon:
    call    lf_shadow

    call    lf_sclk_high
    call    lf_shadow

    call    lf_sclk_high
    call    lf_data4_low
    call    lf_data7_low

    PSHL    0xFF	    ; All rom Profiles are off
    call    strobe0

    PSHL    0xC7	    ; Disables external ram decoder and read buffer
    call    strobe1
    return

;******************************************************
;Description: wice access disabled
; ( -- )
;******************************************************
wice_close:
    PSHL    3		    ; select no strobe latches
    call    lf_sel_latch    ; pop and select strobe latche
    call    lf_led_off	    ; turn green LED off
    call    lf_data7_low    
    call    lf_sclk_high
    call    lf_shadow
    return
    

;****************************************************
; Read in the 8 bits data that sitting U7 buffer
; ( -- d )
;****************************************************
wice_read:
    PSHL    0x46
    call    strobe1
    call    par_read_nibble	    ; we now data on stack

    PSHL    0x86
    call    par_strobe_data
    call    par_read_nibble	    ; so now we have two nibble on stack
    PSHL    0xc7		    ; disable U7
    call    par_strobe_data
    SWAP			    ; high nible first
    call    pf_lsl4		    ; shift left 4
    OR				    ; or high nibble with low nibble
    return


;*****************************************************
; Reset Address Counter
;*****************************************************
wice_reset_address:
    PSHL    0x11	    ;
    call    strobe2
    PSHL    0x01
    call    par_strobe_data   
    return

;****************************************************
; Write to WICE memory B
;****************************************************
wice_write_b:
    call    wice_normalise  ; control latch set to default

    PSHL    1
    call    wice_open		; I want to open emu port b

    call    wice_reset_address	;as it says

    call    wice_read
    DROP			; need to find out what to do with data
    call    par_inc_address	; increment address

    call    wice_read
    DROP

    call    wice_reset_address

    PSHL    0x5A		    ; now put data in RAM
    call    par_selptr_data

    call    wice_read
    DROP

    call    par_inc_address	    ; this should increment address

    PSHL    0xA5		    ; now write A5 to RAM
    call    par_selptr_data

    call    wice_read
    DROP

    call    par_inc_address	    ; this should increment address

    call    wice_reset_address

    call    wice_read
    DROP

    call    par_inc_address	    ; increment address

    call    wice_read
    DROP

    call    wice_reset_address

    PSHL    0xFF
    call    par_selptr_data

    call    par_inc_address		; increment address



    return


;**************************************************
; Write to WICE memory A
;**************************************************
wice_write_a:
    call    wice_normalise  ; control latch set to default

    PSHL    0
    call    wice_open	    ; we open port A here

    call    wice_reset_address
    call    wice_read		; read what ever
    DROP			; toss the results
    call    par_inc_address
    call    wice_read
    DROP
    call    wice_reset_address

    PSHL    0x5A		    ; now put data in RAM
    call    par_selptr_data
    call    wice_read
    DROP
    call    par_inc_address		    ; this should increment address
    PSHL    0xA5		    ; now write A5 to RAM
    call    par_selptr_data
    call    wice_read
    DROP
    call    par_inc_address	    ; this should increment address
    call    wice_reset_address

    call    wice_read
    DROP
    call    par_inc_address		; increment address
    call    wice_read
    DROP
    call    wice_reset_address

    PSHL    0xFF
    call    par_selptr_data
    call    par_inc_address		; increment address
    PSHL    0xFF
    call    par_selptr_data

    call    wice_reset_address
    call    wice_reset_address


; 1. we put data here
; 2. toggle SELPTR
; 3. toggle INIT
; 4. repeat 1,2,3 until the end of data


; end signals
    PSHL    0x95
    call    strobe2

    PSHL    0xc0
    call    strobe1

    PSHL    0x7c
    call    strobe0

    PSHL    3
    call    lf_sel_latch    ; pop and select strobe latche
    call    lf_sclk_low
    call    lf_led_off
    call    lf_pclk_high
    call    lf_data7_low
    call    lf_shadow

    call    lf_sclk_high
    call    lf_shadow

    PSHL    0
    call    lf_sel_latch
    call    lf_led_on
    call    lf_shadow


    return

	
;************************************************
; Desciption: Initilise stuff
;************************************************
wice_init:
    PSHL    0
    call    wice_open
    call    strobe_init	    ; this is the only time we can get access to strobe
    call    wice_close	    ; turn off access to memory

    PSHL    1
    call    wice_open
    call    strobe_init
    call    wice_close
    return
    


    END