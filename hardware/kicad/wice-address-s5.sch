EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 16 22
Title ""
Date "18 sep 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L wice-mod:74LS244-IEEE U20
U 1 1 59AF1CF7
P 5750 3600
F 0 "U20" H 5700 4100 60  0000 C CNN
F 1 "74LS244-IEEE" H 5750 3250 60  0000 C CNN
F 2 "~" H 5750 3600 60  0000 C CNN
F 3 "~" H 5750 3600 60  0000 C CNN
	1    5750 3600
	1    0    0    -1  
$EndComp
Text HLabel 2200 3300 0    60   Input ~ 0
SEL
Wire Wire Line
	2200 3300 5200 3300
Wire Wire Line
	5200 3500 4600 3500
Wire Wire Line
	4600 3500 4600 2700
Text HLabel 2200 3600 0    60   Input ~ 0
XA14
Text HLabel 2200 3700 0    60   Input ~ 0
XCS1
Text HLabel 2200 3800 0    60   Input ~ 0
XWE
Wire Wire Line
	2200 3600 5200 3600
Wire Wire Line
	5200 3700 2200 3700
Wire Wire Line
	2200 3800 5200 3800
Text HLabel 8300 3500 2    60   Output ~ 0
RCS2
Text HLabel 8300 3600 2    60   Output ~ 0
RA14
Text HLabel 8300 3700 2    60   Output ~ 0
RCS3
Text HLabel 8300 3800 2    60   Output ~ 0
RWE
Wire Wire Line
	6300 3500 8300 3500
Wire Wire Line
	6300 3600 8300 3600
Wire Wire Line
	6300 3700 8300 3700
Wire Wire Line
	6300 3800 8300 3800
$Comp
L power:VCC #PWR?
U 1 1 5EC05FA4
P 4600 2700
F 0 "#PWR?" H 4600 2550 50  0001 C CNN
F 1 "VCC" H 4617 2873 50  0000 C CNN
F 2 "" H 4600 2700 50  0001 C CNN
F 3 "" H 4600 2700 50  0001 C CNN
	1    4600 2700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
