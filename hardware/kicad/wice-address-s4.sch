EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 15 22
Title ""
Date "18 sep 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L wice-mod:74LS244-IEEE U18
U 1 1 59AEE825
P 5550 3700
F 0 "U18" H 5550 4150 60  0000 C CNN
F 1 "74LS244-IEEE" H 5500 3350 60  0000 C CNN
F 2 "~" H 5550 3700 60  0000 C CNN
F 3 "~" H 5550 3700 60  0000 C CNN
	1    5550 3700
	1    0    0    -1  
$EndComp
Text HLabel 1700 3400 0    60   Input ~ 0
SEL
Wire Wire Line
	5000 3400 1700 3400
Text HLabel 1700 3600 0    60   Input ~ 0
XA11
Text HLabel 1700 3700 0    60   Input ~ 0
XA12
Text HLabel 1700 3800 0    60   Input ~ 0
XWE
Text HLabel 1700 3900 0    60   Input ~ 0
XCS1
Wire Wire Line
	1700 3600 5000 3600
Wire Wire Line
	5000 3700 1700 3700
Wire Wire Line
	1700 3800 5000 3800
Wire Wire Line
	5000 3900 1700 3900
Text HLabel 8150 3600 2    60   Output ~ 0
RA11
Text HLabel 8150 3700 2    60   Output ~ 0
RA12
Text HLabel 8150 3800 2    60   Output ~ 0
RWE
Text HLabel 8150 3900 2    60   Output ~ 0
RCS3
Wire Wire Line
	6100 3600 8150 3600
Wire Wire Line
	6100 3700 8150 3700
Wire Wire Line
	6100 3800 8150 3800
Wire Wire Line
	8150 3900 6100 3900
$EndSCHEMATC
