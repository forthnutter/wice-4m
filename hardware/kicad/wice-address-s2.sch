EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 13 22
Title ""
Date "18 sep 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L wice-mod:74LS244-IEEE U19
U 1 1 59AE9BC9
P 5600 3000
F 0 "U19" H 5950 3400 60  0000 C CNN
F 1 "74LS244-IEEE" H 5850 2700 60  0000 C CNN
F 2 "~" H 5600 3000 60  0000 C CNN
F 3 "~" H 5600 3000 60  0000 C CNN
	1    5600 3000
	1    0    0    -1  
$EndComp
Text HLabel 1800 2700 0    60   Input ~ 0
SEL
Wire Wire Line
	1800 2700 5050 2700
Text HLabel 1800 2900 0    60   Input ~ 0
XCS1
Text HLabel 1800 3000 0    60   Input ~ 0
XCS2
Wire Wire Line
	5050 2900 1800 2900
Wire Wire Line
	5050 3000 1800 3000
Text HLabel 8700 2900 2    60   Output ~ 0
RCS1
Text HLabel 8700 3000 2    60   Output ~ 0
RCS2
Wire Wire Line
	6150 2900 8700 2900
Wire Wire Line
	6150 3000 8700 3000
$EndSCHEMATC
