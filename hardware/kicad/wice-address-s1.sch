EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 22
Title ""
Date "18 sep 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L wice-mod:74LS244-IEEE U18
U 1 1 59AC4F07
P 5700 1400
F 0 "U18" H 6050 1800 60  0000 C CNN
F 1 "74LS244-IEEE" H 5950 1100 60  0000 C CNN
F 2 "~" H 5700 1400 60  0000 C CNN
F 3 "~" H 5700 1400 60  0000 C CNN
	1    5700 1400
	1    0    0    -1  
$EndComp
Text HLabel 1600 1100 0    60   Input ~ 0
PROFILE1
Wire Wire Line
	1600 1100 5150 1100
Entry Wire Line
	4250 1600 4350 1500
Entry Wire Line
	4250 1500 4350 1400
Entry Wire Line
	4250 1400 4350 1300
Wire Wire Line
	4350 1300 5150 1300
Wire Wire Line
	5150 1400 4350 1400
Wire Wire Line
	4350 1500 5150 1500
Wire Bus Line
	4250 2150 1600 2150
Text HLabel 1600 2150 0    60   Input ~ 0
XA[14..16]
Wire Bus Line
	7150 600  8500 600 
Text HLabel 8500 600  2    60   Output ~ 0
RA[14..16]
Entry Wire Line
	7050 1500 7150 1400
Entry Wire Line
	7050 1400 7150 1300
Entry Wire Line
	7050 1300 7150 1200
Wire Wire Line
	6250 1300 7050 1300
Wire Wire Line
	6250 1400 7050 1400
Wire Wire Line
	6250 1500 7050 1500
Text Label 6650 1300 0    60   ~ 0
RA14
Text Label 6650 1400 0    60   ~ 0
RA15
Text Label 6650 1500 0    60   ~ 0
RA16
Text Label 4450 1300 0    60   ~ 0
XA14
Text Label 4450 1400 0    60   ~ 0
XA15
Text Label 4450 1500 0    60   ~ 0
XA16
Wire Bus Line
	4250 1400 4250 2150
Wire Bus Line
	7150 600  7150 1400
$EndSCHEMATC
