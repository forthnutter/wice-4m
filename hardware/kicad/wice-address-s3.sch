EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 14 22
Title ""
Date "18 sep 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L wice-mod:74LS244-IEEE U19
U 1 1 59AEC157
P 5550 3400
F 0 "U19" H 5550 3850 60  0000 C CNN
F 1 "74LS244-IEEE" H 5550 3050 60  0000 C CNN
F 2 "~" H 5550 3400 60  0000 C CNN
F 3 "~" H 5550 3400 60  0000 C CNN
	1    5550 3400
	1    0    0    -1  
$EndComp
Text HLabel 2000 3100 0    60   Input ~ 0
SEL
Wire Wire Line
	2000 3100 5000 3100
Wire Wire Line
	5000 3300 4500 3300
Wire Wire Line
	4500 3300 4500 2600
Text HLabel 2000 3400 0    60   Input ~ 0
A11
Text HLabel 2000 3500 0    60   Input ~ 0
XCS1
Wire Wire Line
	2000 3400 5000 3400
Wire Wire Line
	2000 3500 5000 3500
Text HLabel 9100 3300 2    60   Output ~ 0
RCS2
Text HLabel 9100 3400 2    60   Output ~ 0
RWE
Text HLabel 9100 3500 2    60   Output ~ 0
RCS1
Wire Wire Line
	6100 3300 9100 3300
Wire Wire Line
	9100 3400 6100 3400
Wire Wire Line
	6100 3500 9100 3500
$Comp
L power:VCC #PWR?
U 1 1 5EC04C2C
P 4500 2600
F 0 "#PWR?" H 4500 2450 50  0001 C CNN
F 1 "VCC" H 4517 2773 50  0000 C CNN
F 2 "" H 4500 2600 50  0001 C CNN
F 3 "" H 4500 2600 50  0001 C CNN
	1    4500 2600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
