EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 22
Title ""
Date "18 sep 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS374 U4
U 1 1 58668123
P 5300 1250
F 0 "U4" H 5300 1250 60  0000 C CNN
F 1 "74LS374" H 5350 900 60  0000 C CNN
F 2 "~" H 5300 1250 60  0000 C CNN
F 3 "~" H 5300 1250 60  0000 C CNN
	1    5300 1250
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS374 U5
U 1 1 58668132
P 4800 2800
F 0 "U5" H 4800 2800 60  0000 C CNN
F 1 "74LS374" H 4850 2450 60  0000 C CNN
F 2 "~" H 4800 2800 60  0000 C CNN
F 3 "~" H 4800 2800 60  0000 C CNN
	1    4800 2800
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS273 U6
U 1 1 58668141
P 5300 5200
F 0 "U6" H 5300 5050 60  0000 C CNN
F 1 "74LS273" H 5300 4850 60  0000 C CNN
F 2 "~" H 5300 5200 60  0000 C CNN
F 3 "~" H 5300 5200 60  0000 C CNN
	1    5300 5200
	1    0    0    -1  
$EndComp
Entry Wire Line
	3550 850  3650 750 
Entry Wire Line
	3550 950  3650 850 
Entry Wire Line
	3550 1050 3650 950 
Entry Wire Line
	3550 1150 3650 1050
Entry Wire Line
	3550 1250 3650 1150
Entry Wire Line
	3550 1350 3650 1250
Entry Wire Line
	3550 1450 3650 1350
Entry Wire Line
	3550 1550 3650 1450
Entry Wire Line
	3550 2200 3650 2300
Entry Wire Line
	3550 2300 3650 2400
Entry Wire Line
	3550 2400 3650 2500
Entry Wire Line
	3550 2500 3650 2600
Entry Wire Line
	3550 2600 3650 2700
Entry Wire Line
	3550 2700 3650 2800
Entry Wire Line
	3550 2800 3650 2900
Entry Wire Line
	3550 2900 3650 3000
Entry Wire Line
	3550 5300 3650 5400
Entry Wire Line
	3550 5200 3650 5300
Entry Wire Line
	3550 5100 3650 5200
Entry Wire Line
	3550 5000 3650 5100
Entry Wire Line
	3550 4900 3650 5000
Entry Wire Line
	3550 4800 3650 4900
Entry Wire Line
	3550 4700 3650 4800
Entry Wire Line
	3550 4600 3650 4700
Wire Wire Line
	3650 5400 4800 5400
Wire Wire Line
	4800 5300 3650 5300
Wire Wire Line
	3650 5200 4800 5200
Wire Wire Line
	4800 5100 3650 5100
Wire Wire Line
	3650 5000 4800 5000
Wire Wire Line
	4800 4900 3650 4900
Wire Wire Line
	3650 4800 4800 4800
Wire Wire Line
	4800 4700 3650 4700
Wire Wire Line
	3650 3000 4300 3000
Wire Wire Line
	4300 2900 3650 2900
Wire Wire Line
	3650 2800 4300 2800
Wire Wire Line
	4300 2700 3650 2700
Wire Wire Line
	3650 2600 4300 2600
Wire Wire Line
	4300 2500 3650 2500
Wire Wire Line
	3650 2400 4300 2400
Wire Wire Line
	4300 2300 3650 2300
Wire Wire Line
	4800 1450 3650 1450
Wire Wire Line
	3650 1350 4800 1350
Wire Wire Line
	4800 1250 3650 1250
Wire Wire Line
	3650 1150 4800 1150
Wire Wire Line
	4800 1050 3650 1050
Wire Wire Line
	3650 950  4800 950 
Wire Wire Line
	4800 850  3650 850 
Wire Wire Line
	3650 750  4800 750 
Text Label 3700 750  0    60   ~ 0
BD0
Text Label 3700 850  0    60   ~ 0
BD1
Text Label 3700 950  0    60   ~ 0
BD2
Text Label 3700 1050 0    60   ~ 0
BD3
Text Label 3700 1150 0    60   ~ 0
BD4
Text Label 3700 1250 0    60   ~ 0
BD5
Text Label 3700 1350 0    60   ~ 0
BD6
Text Label 3700 1450 0    60   ~ 0
BD7
Text Label 3700 4700 0    60   ~ 0
BD0
Text Label 3700 4800 0    60   ~ 0
BD1
Text Label 3700 4900 0    60   ~ 0
BD2
Text Label 3700 5000 0    60   ~ 0
BD3
Text Label 3700 5100 0    60   ~ 0
BD4
Text Label 3700 5200 0    60   ~ 0
BD5
Text Label 3700 5300 0    60   ~ 0
BD6
Text Label 3700 5400 0    60   ~ 0
BD7
Text Label 3700 2300 0    60   ~ 0
BD0
Text Label 3700 2400 0    60   ~ 0
BD1
Text Label 3700 2500 0    60   ~ 0
BD2
Text Label 3700 2600 0    60   ~ 0
BD3
Text Label 3700 2700 0    60   ~ 0
BD4
Text Label 3700 2800 0    60   ~ 0
BD5
Text Label 3700 2900 0    60   ~ 0
BD6
Text Label 3700 3000 0    60   ~ 0
BD7
Text HLabel 2250 1750 0    60   Input ~ 0
LOE
Wire Wire Line
	2250 1750 3400 1750
Text HLabel 2250 1650 0    60   Input ~ 0
LATCH0
Wire Wire Line
	4800 1650 2250 1650
Text HLabel 2250 1950 0    60   Input ~ 0
BD[0..7]
Wire Bus Line
	2250 1950 3550 1950
$Comp
L wice-mod:RR8 RP8
U 1 1 58690AF6
P 6750 2000
F 0 "RP8" V 6750 2700 70  0000 C CNN
F 1 "RR8" V 6780 2000 70  0000 C CNN
F 2 "~" H 6750 2000 60  0000 C CNN
F 3 "~" H 6750 2000 60  0000 C CNN
	1    6750 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 750  7100 750 
Wire Wire Line
	7100 750  7100 1650
Text HLabel 9600 2300 2    60   Output ~ 0
RAMOE
Connection ~ 7100 750 
$Comp
L power:VCC #PWR?
U 1 1 5869117A
P 7200 1600
F 0 "#PWR?" H 7200 1700 30  0001 C CNN
F 1 "VCC" H 7250 1700 30  0000 C CNN
F 2 "" H 7200 1600 60  0000 C CNN
F 3 "" H 7200 1600 60  0000 C CNN
	1    7200 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 1650 7200 1600
Text HLabel 2250 3200 0    60   Input ~ 0
LATCH1
Text HLabel 2250 5600 0    60   Input ~ 0
LATCH2
Wire Wire Line
	2250 3200 4300 3200
Wire Wire Line
	2250 5600 4800 5600
Wire Wire Line
	4300 3300 3400 3300
Wire Wire Line
	3400 3300 3400 1750
Connection ~ 3400 1750
Text HLabel 2250 5700 0    60   Input ~ 0
RESET
Wire Wire Line
	2250 5700 4800 5700
Text HLabel 9600 5100 2    60   Output ~ 0
ARESET
Wire Wire Line
	5800 5100 9600 5100
Text HLabel 9600 3000 2    60   Output ~ 0
UNIBBLE
Text HLabel 9600 2900 2    60   Output ~ 0
LNIBBLE
Wire Wire Line
	5300 3000 8350 3000
Wire Wire Line
	5300 2900 8150 2900
Wire Wire Line
	5300 2300 8250 2300
$Comp
L wice-mod:RR8 RP9
U 1 1 5996EDDC
P 8000 1600
F 0 "RP9" V 8000 2300 70  0000 C CNN
F 1 "RR8" V 8030 1600 70  0000 C CNN
F 2 "~" H 8000 1600 60  0000 C CNN
F 3 "~" H 8000 1600 60  0000 C CNN
	1    8000 1600
	0    1    -1   0   
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5996EDFD
P 9050 1650
F 0 "#PWR?" H 9050 1750 30  0001 C CNN
F 1 "VCC" H 9100 1750 30  0000 C CNN
F 2 "" H 9050 1650 60  0000 C CNN
F 3 "" H 9050 1650 60  0000 C CNN
	1    9050 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 1950 8450 2050
Wire Wire Line
	8450 2050 9050 2050
Wire Wire Line
	9050 2050 9050 1650
Wire Wire Line
	8250 1950 8250 2300
Connection ~ 8250 2300
Wire Wire Line
	8350 1950 8350 3000
Connection ~ 8350 3000
Wire Wire Line
	8150 1950 8150 2900
Connection ~ 8150 2900
Text HLabel 9600 750  2    60   Output ~ 0
PROFILE0
Text HLabel 9600 850  2    60   Output ~ 0
PROFILE1
Wire Wire Line
	9600 850  7000 850 
Wire Wire Line
	7000 1650 7000 850 
Connection ~ 7000 850 
Text HLabel 9600 950  2    60   Output ~ 0
PROFILE2
Wire Wire Line
	9600 950  6900 950 
Wire Wire Line
	6900 1650 6900 950 
Connection ~ 6900 950 
Text HLabel 9600 1050 2    60   Output ~ 0
PROFILE3
Text HLabel 9600 1150 2    60   Output ~ 0
PROFILE4
Text HLabel 9600 1250 2    60   Output ~ 0
PROFILE5
Text HLabel 9600 1350 2    60   Output ~ 0
PROFILE6
Text HLabel 9600 1450 2    60   Output ~ 0
PROFILE7
Wire Wire Line
	5800 1050 6800 1050
Wire Wire Line
	6800 1650 6800 1050
Connection ~ 6800 1050
Wire Wire Line
	9600 1150 6700 1150
Wire Wire Line
	6700 1650 6700 1150
Connection ~ 6700 1150
Wire Wire Line
	9600 1250 6600 1250
Wire Wire Line
	6600 1250 6650 1250
Wire Wire Line
	6600 1650 6600 1250
Connection ~ 6600 1250
Wire Wire Line
	9600 1350 6500 1350
Wire Wire Line
	9600 1450 6400 1450
Wire Wire Line
	6500 1650 6500 1350
Connection ~ 6500 1350
Wire Wire Line
	6400 1650 6400 1450
Connection ~ 6400 1450
Wire Wire Line
	5300 2400 7950 2400
Text HLabel 9600 2400 2    60   Output ~ 0
EMUSEL
Wire Wire Line
	7950 1950 7950 2400
Connection ~ 7950 2400
Text HLabel 9600 2500 2    60   Output ~ 0
BSEL
Wire Wire Line
	5300 2500 7750 2500
Text HLabel 9600 2600 2    60   Output ~ 0
ENA17
Wire Wire Line
	5300 2600 6350 2600
$Comp
L Device:R R15
U 1 1 599C22C0
P 6350 3850
F 0 "R15" V 6430 3850 40  0000 C CNN
F 1 "R" V 6357 3851 40  0000 C CNN
F 2 "~" V 6280 3850 30  0000 C CNN
F 3 "~" H 6350 3850 30  0000 C CNN
	1    6350 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 599C22DB
P 6350 4150
AR Path="/599C22DB" Ref="#PWR?"  Part="1" 
AR Path="/58667DAF/599C22DB" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6350 4150 30  0001 C CNN
F 1 "GND" H 6350 4080 30  0001 C CNN
F 2 "" H 6350 4150 60  0000 C CNN
F 3 "" H 6350 4150 60  0000 C CNN
	1    6350 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4150 6350 4000
Wire Wire Line
	6350 3700 6350 2600
Connection ~ 6350 2600
Wire Wire Line
	7750 1950 7750 2500
Connection ~ 7750 2500
Text HLabel 9600 2700 2    60   Output ~ 0
ERRCLR
Wire Wire Line
	5300 2700 7850 2700
Wire Wire Line
	7850 1950 7850 2700
Connection ~ 7850 2700
Text HLabel 9600 2800 2    60   Output ~ 0
ESTATUS
Wire Wire Line
	5300 2800 8050 2800
Wire Wire Line
	8050 1950 8050 2800
Connection ~ 8050 2800
Text HLabel 9600 4700 2    60   Output ~ 0
L2D0
Wire Wire Line
	5800 4700 9600 4700
Text HLabel 9600 4800 2    60   Output ~ 0
ENA15
Wire Wire Line
	5800 4800 9600 4800
Text HLabel 9600 4900 2    60   Output ~ 0
ENDATA
Wire Wire Line
	5800 4900 9600 4900
Text HLabel 9600 5000 2    60   Output ~ 0
ENCS2
Wire Wire Line
	5800 5000 9600 5000
Text HLabel 9600 5200 2    60   Output ~ 0
EN-A17
Wire Wire Line
	5800 5200 9600 5200
Text HLabel 9600 5400 2    60   Output ~ 0
ENRAM0
Text HLabel 9600 5300 2    60   Output ~ 0
ENRAM1
Wire Wire Line
	5800 5300 9600 5300
Wire Wire Line
	5800 5400 9600 5400
Wire Wire Line
	7100 750  9600 750 
Wire Wire Line
	3400 1750 4800 1750
Wire Wire Line
	8250 2300 9600 2300
Wire Wire Line
	8350 3000 9600 3000
Wire Wire Line
	8150 2900 9600 2900
Wire Wire Line
	7000 850  5800 850 
Wire Wire Line
	6900 950  5800 950 
Wire Wire Line
	6800 1050 9600 1050
Wire Wire Line
	6700 1150 5800 1150
Wire Wire Line
	6600 1250 5800 1250
Wire Wire Line
	6500 1350 5800 1350
Wire Wire Line
	6400 1450 5800 1450
Wire Wire Line
	7950 2400 9600 2400
Wire Wire Line
	6350 2600 9600 2600
Wire Wire Line
	7750 2500 9600 2500
Wire Wire Line
	7850 2700 9600 2700
Wire Wire Line
	8050 2800 9600 2800
Wire Bus Line
	3550 850  3550 5300
$EndSCHEMATC
