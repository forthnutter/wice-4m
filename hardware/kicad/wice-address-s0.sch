EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 22
Title ""
Date "18 sep 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Entry Wire Line
	9350 1550 9450 1650
Entry Wire Line
	9350 1650 9450 1750
Entry Wire Line
	9350 1750 9450 1850
Entry Wire Line
	9350 1850 9450 1950
Entry Wire Line
	9350 2900 9450 3000
Entry Wire Line
	9350 3000 9450 3100
Entry Wire Line
	9350 3100 9450 3200
Entry Wire Line
	9350 3200 9450 3300
Entry Wire Line
	9350 4300 9450 4200
Entry Wire Line
	9350 4400 9450 4300
Entry Wire Line
	9350 4500 9450 4400
Wire Wire Line
	6050 4300 9350 4300
Wire Wire Line
	6050 4400 9350 4400
Wire Wire Line
	6050 4500 9350 4500
Wire Wire Line
	6050 3200 9350 3200
Wire Wire Line
	6050 3100 9350 3100
Wire Wire Line
	6050 3000 9350 3000
Wire Wire Line
	6050 2900 9350 2900
Wire Wire Line
	6050 1850 9350 1850
Wire Wire Line
	6050 1750 9350 1750
Wire Wire Line
	6050 1650 9350 1650
Wire Wire Line
	6050 1550 9350 1550
Text Label 8850 1550 2    60   ~ 0
XA0
Text Label 8850 1650 2    60   ~ 0
XA1
Text Label 8850 1750 2    60   ~ 0
XA2
Text Label 8850 1850 2    60   ~ 0
XA3
Text Label 8850 2900 2    60   ~ 0
XA4
Text Label 8850 3000 2    60   ~ 0
XA5
Text Label 8850 3100 2    60   ~ 0
XA6
Text Label 8850 3200 2    60   ~ 0
XA7
Text Label 8850 4300 2    60   ~ 0
XA8
Text Label 8850 4400 2    60   ~ 0
XA9
Text Label 8850 4500 2    60   ~ 0
XA10
Text HLabel 10350 2250 2    60   Input ~ 0
XA[0..10]
Wire Bus Line
	10350 2250 9450 2250
Text HLabel 10350 4600 2    60   Input ~ 0
XOE
Entry Wire Line
	2600 1650 2700 1550
Entry Wire Line
	2600 1750 2700 1650
Entry Wire Line
	2600 1850 2700 1750
Entry Wire Line
	2600 1950 2700 1850
Entry Wire Line
	2600 3300 2700 3200
Entry Wire Line
	2600 3200 2700 3100
Entry Wire Line
	2600 3100 2700 3000
Entry Wire Line
	2600 3000 2700 2900
Wire Wire Line
	2700 1550 4950 1550
Wire Wire Line
	2700 1650 4950 1650
Wire Wire Line
	2700 1750 4950 1750
Wire Wire Line
	2700 1850 4950 1850
Entry Wire Line
	2600 4400 2700 4500
Entry Wire Line
	2600 4300 2700 4400
Entry Wire Line
	2600 4200 2700 4300
Wire Wire Line
	2700 4500 4950 4500
Text HLabel 1400 4600 0    60   Output ~ 0
ROE
Wire Wire Line
	1400 4600 4950 4600
Text HLabel 1350 1000 0    60   Input ~ 0
PROFILE0
Text HLabel 1350 2400 0    60   Output ~ 0
RA[0..10]
Wire Bus Line
	1350 2400 2600 2400
Text Label 2950 1550 2    60   ~ 0
RA0
Text Label 2950 1650 2    60   ~ 0
RA1
Text Label 2950 1750 2    60   ~ 0
RA2
Text Label 2950 1850 2    60   ~ 0
RA3
Text Label 2950 2900 2    60   ~ 0
RA4
Text Label 2950 3000 2    60   ~ 0
RA5
Text Label 2950 3100 2    60   ~ 0
RA6
Text Label 2950 3200 2    60   ~ 0
RA7
Text Label 3050 4300 2    60   ~ 0
RA8
Text Label 3050 4400 2    60   ~ 0
RA9
Text Label 3100 4500 2    60   ~ 0
RA10
Text Notes 2550 5750 2    60   ~ 0
PROFILE0 = 2716 EPROM
$Comp
L wice-mod:74LS244-IEEE U16
U 2 1 599CDE0A
P 5500 1650
F 0 "U16" H 5500 2100 60  0000 C CNN
F 1 "74LS244-IEEE" H 5500 1350 60  0000 C CNN
F 2 "" H 5500 1650 60  0000 C CNN
F 3 "" H 5500 1650 60  0000 C CNN
	2    5500 1650
	-1   0    0    -1  
$EndComp
$Comp
L wice-mod:74LS244-IEEE U16
U 1 1 599CDE50
P 5500 3000
F 0 "U16" H 5500 3450 60  0000 C CNN
F 1 "74LS244-IEEE" H 5500 2700 60  0000 C CNN
F 2 "" H 5500 3000 60  0000 C CNN
F 3 "" H 5500 3000 60  0000 C CNN
	1    5500 3000
	-1   0    0    -1  
$EndComp
$Comp
L wice-mod:74LS244-IEEE U17
U 1 1 599CDEA8
P 5500 4400
F 0 "U17" H 5500 4850 60  0000 C CNN
F 1 "74LS244-IEEE" H 5500 4100 60  0000 C CNN
F 2 "" H 5500 4400 60  0000 C CNN
F 3 "" H 5500 4400 60  0000 C CNN
	1    5500 4400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2700 4400 4950 4400
Wire Wire Line
	4950 4300 2700 4300
Wire Wire Line
	4950 3200 2700 3200
Wire Wire Line
	2700 3100 4950 3100
Wire Wire Line
	4950 3000 2700 3000
Wire Wire Line
	2700 2900 4950 2900
Wire Wire Line
	6050 4600 10350 4600
Wire Wire Line
	1350 1000 6450 1000
Wire Wire Line
	6450 1000 6450 1350
Wire Wire Line
	6450 4100 6050 4100
Wire Wire Line
	6050 1350 6450 1350
Connection ~ 6450 1350
Wire Wire Line
	6050 2700 6450 2700
Connection ~ 6450 2700
Wire Wire Line
	6450 1350 6450 2700
Wire Wire Line
	6450 2700 6450 4100
Wire Bus Line
	9450 1550 9450 4400
Wire Bus Line
	2600 1550 2600 4400
$EndSCHEMATC
