EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 22
Title ""
Date "18 sep 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L wice-mod:IDC-34 CON2
U 1 1 5868B4DB
P 10200 3150
F 0 "CON2" H 10200 4900 70  0000 C CNN
F 1 "IDC-34" H 10150 1250 70  0000 C CNN
F 2 "~" H 10200 4000 60  0000 C CNN
F 3 "~" H 10200 4000 60  0000 C CNN
	1    10200 3150
	-1   0    0    -1  
$EndComp
$Sheet
S 4850 650  1200 650 
U 5868B511
F0 "Address input option 0" 50
F1 "wice-address-s0.sch" 50
F2 "XOE" I R 6050 1000 60 
F3 "ROE" O L 4850 1000 60 
F4 "PROFILE0" I L 4850 800 60 
F5 "RA[0..10]" O L 4850 1150 60 
F6 "XA[0..10]" I R 6050 1150 60 
$EndSheet
Entry Bus Bus
	7400 1150 7500 1250
Wire Bus Line
	7400 1150 6050 1150
Text GLabel 8550 1750 0    60   Input ~ 0
XVCC
Wire Wire Line
	8550 1750 9600 1750
Entry Wire Line
	7500 1750 7600 1850
Entry Wire Line
	7500 1850 7600 1950
Entry Wire Line
	7500 1950 7600 2050
Entry Wire Line
	7500 2750 7600 2850
Entry Wire Line
	7500 2150 7600 2250
Entry Wire Line
	7500 2850 7600 2950
Entry Wire Line
	7500 2350 7600 2450
Entry Wire Line
	7500 2450 7600 2550
Entry Wire Line
	7500 2550 7600 2650
Entry Wire Line
	7500 2650 7600 2750
Wire Wire Line
	9600 1850 7600 1850
Wire Wire Line
	7600 1950 9600 1950
Wire Wire Line
	9600 2050 7600 2050
Wire Wire Line
	9600 2250 7600 2250
Wire Wire Line
	9600 2450 7600 2450
Text Label 7700 1850 0    60   ~ 0
XA17
Text Label 7700 1950 0    60   ~ 0
XA15
Text Label 7700 2050 0    60   ~ 0
XA16
Text Label 7900 2250 2    60   ~ 0
XA14
Wire Wire Line
	9600 2350 8400 2350
Text Label 7900 2450 2    60   ~ 0
XA12
Wire Wire Line
	9600 2550 7600 2550
Text Label 7900 2550 2    60   ~ 0
XA13
Wire Wire Line
	9600 2650 7600 2650
Text Label 7850 2650 2    60   ~ 0
XA7
Wire Wire Line
	7600 2750 9600 2750
Text Label 7850 2750 2    60   ~ 0
XA8
Wire Wire Line
	9600 2850 7600 2850
Text Label 7850 2850 2    60   ~ 0
XA6
Wire Wire Line
	9600 2950 7600 2950
Text Label 7850 2950 2    60   ~ 0
XA9
Entry Wire Line
	7500 2950 7600 3050
Entry Wire Line
	7500 3050 7600 3150
Entry Wire Line
	7500 3150 7600 3250
Entry Wire Line
	7500 3750 7600 3850
Entry Wire Line
	7500 3350 7600 3450
Entry Wire Line
	7500 3450 7600 3550
Entry Wire Line
	7500 3550 7600 3650
Wire Wire Line
	9600 3050 7600 3050
Text Label 7850 3050 2    60   ~ 0
XA5
Wire Wire Line
	9600 3150 7600 3150
Text Label 7900 3150 2    60   ~ 0
XA11
Wire Wire Line
	9600 3250 7600 3250
Text Label 7850 3250 2    60   ~ 0
XA4
Wire Wire Line
	8950 3350 9600 3350
Wire Wire Line
	9600 3450 7600 3450
Text Label 7850 3450 2    60   ~ 0
XA3
Wire Wire Line
	9600 3550 7600 3550
Text Label 7900 3550 2    60   ~ 0
XA10
Wire Wire Line
	9600 3650 7600 3650
Text Label 7850 3650 2    60   ~ 0
XA2
Wire Wire Line
	9600 3750 8950 3750
Wire Wire Line
	7600 3850 9600 3850
Text Label 7700 3850 0    60   ~ 0
XA1
Entry Wire Line
	7500 3950 7600 4050
Text Label 7700 4050 0    60   ~ 0
XA0
Wire Wire Line
	9600 4850 9400 4850
Wire Wire Line
	9400 4850 9400 5150
Entry Wire Line
	8000 4050 8100 3950
Wire Wire Line
	9600 3950 8100 3950
Text Label 8150 3950 0    60   ~ 0
XD7
Entry Wire Line
	8000 4250 8100 4150
Entry Wire Line
	8000 4350 8100 4250
Entry Wire Line
	8000 4450 8100 4350
Entry Wire Line
	8000 4550 8100 4450
Entry Wire Line
	8000 4650 8100 4550
Entry Wire Line
	8000 4750 8100 4650
Entry Wire Line
	8000 4850 8100 4750
Wire Wire Line
	9600 4150 8100 4150
Wire Wire Line
	9600 4250 8100 4250
Wire Wire Line
	9600 4350 8100 4350
Wire Wire Line
	9600 4450 8100 4450
Wire Wire Line
	9600 4550 8100 4550
Wire Wire Line
	9600 4650 8100 4650
Wire Wire Line
	9600 4750 8100 4750
Text Label 8150 4150 0    60   ~ 0
XD6
Text Label 8150 4250 0    60   ~ 0
XD0
Text Label 8150 4350 0    60   ~ 0
XD5
Text Label 8150 4450 0    60   ~ 0
XD1
Text Label 8150 4550 0    60   ~ 0
XD4
Text Label 8150 4650 0    60   ~ 0
XD2
Text Label 8150 4750 0    60   ~ 0
XD3
Wire Wire Line
	6050 1000 8950 1000
Wire Wire Line
	8950 1000 8950 3350
Text HLabel 1200 1950 0    60   Output ~ 0
RA[0..16]
Wire Bus Line
	1200 1950 2800 1950
Entry Bus Bus
	2800 1250 2900 1150
Wire Bus Line
	2900 1150 4850 1150
Text HLabel 1200 800  0    60   Input ~ 0
PROFILE0
Wire Wire Line
	4850 800  1200 800 
$Sheet
S 4850 1600 1200 300 
U 599CBD5F
F0 "Address input option 1" 50
F1 "wice-address-s1.sch" 50
F2 "PROFILE1" I L 4850 1700 60 
F3 "XA[14..16]" I R 6050 1700 60 
F4 "RA[14..16]" I L 4850 1800 60 
$EndSheet
Text HLabel 1200 1000 0    60   Output ~ 0
ROE
Wire Wire Line
	4850 1000 1200 1000
Text HLabel 1200 6700 0    60   Input ~ 0
BSEL
Wire Wire Line
	1200 6700 4850 6700
Text HLabel 1200 1700 0    60   Input ~ 0
PROFILE1
Wire Wire Line
	1200 1700 4850 1700
Wire Bus Line
	6050 1700 7500 1700
Wire Bus Line
	4850 1800 2800 1800
$Sheet
S 4850 2100 1200 450 
U 59AE9880
F0 "Address input option 2" 50
F1 "wice-address-s2.sch" 50
F2 "SEL" I L 4850 2200 60 
F3 "XCS1" I R 6050 2350 60 
F4 "XCS2" I R 6050 2450 60 
F5 "RCS1" O L 4850 2350 60 
F6 "RCS2" O L 4850 2450 60 
$EndSheet
Text Label 9250 3750 2    60   ~ 0
XCS1
Wire Wire Line
	6050 2350 6900 2350
Text Label 6500 2350 2    60   ~ 0
XCS1
Text Label 8850 2150 2    60   ~ 0
XCS2
Wire Wire Line
	6050 2450 6700 2450
Text Label 6500 2450 2    60   ~ 0
XCS2
Text HLabel 1200 2350 0    60   Output ~ 0
RCS1
Text HLabel 1200 2450 0    60   Output ~ 0
RCS2
Wire Wire Line
	1200 2350 3800 2350
Wire Wire Line
	1200 2450 3900 2450
Text HLabel 1200 2200 0    60   Input ~ 0
PROFILE2
Wire Wire Line
	1200 2200 4850 2200
$Sheet
S 4850 2800 1200 500 
U 59AEBD75
F0 "Address input option 3" 50
F1 "wice-address-s3.sch" 50
F2 "SEL" I L 4850 2900 60 
F3 "A11" I R 6050 3100 60 
F4 "XCS1" I R 6050 3200 60 
F5 "RCS2" O L 4850 3000 60 
F6 "RWE" O L 4850 3100 60 
F7 "RCS1" O L 4850 3200 60 
$EndSheet
Entry Wire Line
	7400 3100 7500 3000
Wire Wire Line
	6050 3100 7400 3100
Text Label 7300 3100 2    60   ~ 0
XA11
Wire Wire Line
	6050 3200 6900 3200
Text Label 6450 3200 2    60   ~ 0
XCS1
Wire Wire Line
	4850 3000 3900 3000
Wire Wire Line
	3900 2450 3900 3000
Connection ~ 3900 2450
Wire Wire Line
	4850 3200 3800 3200
Wire Wire Line
	3800 3200 3800 2350
Connection ~ 3800 2350
Text HLabel 1200 3100 0    60   Output ~ 0
RWE
Wire Wire Line
	1200 3100 3700 3100
Text HLabel 1200 2900 0    60   Input ~ 0
PROFILE3
Wire Wire Line
	1200 2900 4850 2900
$Sheet
S 4850 3500 1200 600 
U 59AEE51B
F0 "Address input option 4" 50
F1 "wice-address-s4.sch" 50
F2 "SEL" I L 4850 3600 60 
F3 "XA11" I R 6050 3700 60 
F4 "XA12" I R 6050 3800 60 
F5 "XWE" I R 6050 3900 60 
F6 "XCS1" I R 6050 4000 60 
F7 "RA11" O L 4850 3700 60 
F8 "RA12" O L 4850 3800 60 
F9 "RWE" O L 4850 3900 60 
F10 "RCS3" O L 4850 4000 60 
$EndSheet
Entry Wire Line
	7400 3700 7500 3600
Entry Wire Line
	7400 3800 7500 3700
Wire Wire Line
	6050 3700 7400 3700
Wire Wire Line
	6050 3800 7400 3800
Text Label 8850 2350 2    60   ~ 0
XWE
Wire Wire Line
	6900 2350 6900 3200
Wire Wire Line
	6900 4000 6050 4000
Connection ~ 6900 3200
Wire Wire Line
	6050 3900 6800 3900
Text Label 6450 3900 2    60   ~ 0
XWE
Text Label 6450 4000 2    60   ~ 0
XCS1
Text HLabel 1200 3600 0    60   Input ~ 0
PROFILE4
Entry Wire Line
	2800 3600 2900 3700
Entry Wire Line
	2800 3700 2900 3800
Wire Wire Line
	4850 3700 2900 3700
Wire Wire Line
	4850 3800 2900 3800
Wire Wire Line
	4850 3900 3700 3900
Wire Wire Line
	3700 3100 3700 3900
Connection ~ 3700 3100
Text HLabel 1200 4000 0    60   Output ~ 0
RCS3
Wire Wire Line
	1200 4000 3600 4000
Text Label 3000 3700 0    60   ~ 0
RA11
Text Label 3000 3800 0    60   ~ 0
RA12
$Sheet
S 4850 4300 1200 550 
U 59AF1B9F
F0 "Address input option" 50
F1 "wice-address-s5.sch" 50
F2 "SEL" I L 4850 4400 60 
F3 "XA14" I R 6050 4600 60 
F4 "XCS1" I R 6050 4700 60 
F5 "XWE" I R 6050 4800 60 
F6 "RCS2" O L 4850 4500 60 
F7 "RA14" O L 4850 4600 60 
F8 "RCS3" O L 4850 4700 60 
F9 "RWE" O L 4850 4800 60 
$EndSheet
Text HLabel 1200 4400 0    60   Input ~ 0
PROFILE5
Wire Wire Line
	4850 4400 1200 4400
Wire Wire Line
	3900 4500 4850 4500
Connection ~ 3900 3000
Entry Wire Line
	2800 4500 2900 4600
Wire Wire Line
	2900 4600 4850 4600
Text Label 3000 4600 0    60   ~ 0
RA14
Wire Wire Line
	4850 4700 3600 4700
Wire Wire Line
	3600 4700 3600 4000
Connection ~ 3600 4000
Wire Wire Line
	3700 4800 4850 4800
Connection ~ 3700 3900
Entry Wire Line
	7400 4600 7500 4500
Wire Wire Line
	6050 4600 7400 4600
Text Label 7150 4600 0    60   ~ 0
XA14
Wire Wire Line
	6900 4700 6050 4700
Connection ~ 6900 4000
Wire Wire Line
	6050 4800 6800 4800
Wire Wire Line
	6800 3900 6800 4800
Text Label 6300 4800 0    60   ~ 0
XWE
Text Label 6300 4700 0    60   ~ 0
XCS1
$Sheet
S 4850 5100 1200 550 
U 59AF4B8E
F0 "Address input option 6" 50
F1 "wice-address-s6.sch" 50
F2 "SEL" I L 4850 5200 60 
F3 "XA14" I R 6050 5300 60 
F4 "XA15" I R 6050 5400 60 
F5 "XA16" I R 6050 5500 60 
F6 "XWE" I R 6050 5600 60 
F7 "RA14" O L 4850 5300 60 
F8 "RA15" O L 4850 5400 60 
F9 "RA16" O L 4850 5500 60 
F10 "RWE" O L 4850 5600 60 
$EndSheet
Text HLabel 1200 5200 0    60   Input ~ 0
PROFILE6
Entry Wire Line
	2800 5200 2900 5300
Entry Wire Line
	2800 5300 2900 5400
Entry Wire Line
	2800 5400 2900 5500
Wire Wire Line
	4850 5300 2900 5300
Wire Wire Line
	2900 5400 4850 5400
Wire Wire Line
	4850 5500 2900 5500
Wire Wire Line
	3700 5600 4850 5600
Connection ~ 3700 4800
Text Label 3000 5300 0    60   ~ 0
RA14
Text Label 3000 5400 0    60   ~ 0
RA15
Text Label 3000 5500 0    60   ~ 0
RA16
Entry Wire Line
	7400 5300 7500 5200
Entry Wire Line
	7400 5400 7500 5300
Entry Wire Line
	7400 5500 7500 5400
Wire Wire Line
	6050 5300 7400 5300
Wire Wire Line
	6050 5400 6700 5400
Wire Wire Line
	6050 5500 7400 5500
Text Label 7150 5300 0    60   ~ 0
XA14
Text Label 7150 5400 0    60   ~ 0
XA15
Text Label 7150 5500 0    60   ~ 0
XA16
Wire Wire Line
	6800 5600 6050 5600
Connection ~ 6800 4800
Text Label 6350 5600 0    60   ~ 0
XWE
$Sheet
S 4850 5850 1200 500 
U 59AF9ABF
F0 "Address input option 7" 50
F1 "wice-address-s7.sch" 50
F2 "SEL" I L 4850 5950 60 
F3 "XA11" I R 6050 6100 60 
F4 "XA12" I R 6050 6200 60 
F5 "XA13" I R 6050 6300 60 
F6 "RA11" O L 4850 6100 60 
F7 "RA12" O L 4850 6200 60 
F8 "RA13" O L 4850 6300 60 
$EndSheet
Entry Wire Line
	2800 6200 2900 6300
Entry Wire Line
	2800 6100 2900 6200
Entry Wire Line
	2800 6000 2900 6100
Wire Wire Line
	2900 6100 4850 6100
Wire Wire Line
	2900 6200 4850 6200
Wire Wire Line
	4850 6300 2900 6300
Text HLabel 1200 5950 0    60   Input ~ 0
PROFILE7
Wire Wire Line
	1200 5950 4850 5950
Text Label 3000 6100 0    60   ~ 0
RA11
Text Label 3000 6200 0    60   ~ 0
RA12
Text Label 3000 6300 0    60   ~ 0
RA13
Entry Wire Line
	7400 6300 7500 6200
Entry Wire Line
	7400 6200 7500 6100
Entry Wire Line
	7400 6100 7500 6000
Wire Wire Line
	6050 6100 7400 6100
Wire Wire Line
	6050 6200 7400 6200
Wire Wire Line
	6050 6300 7400 6300
Text Label 7150 6100 0    60   ~ 0
XA11
Text Label 7150 6200 0    60   ~ 0
XA12
Text Label 7150 6300 0    60   ~ 0
XA13
Text Label 7100 3700 0    60   ~ 0
XA11
Text Label 7100 3800 0    60   ~ 0
XA12
$Sheet
S 4850 6600 1200 1050
U 59B106D1
F0 "Address input option 8" 50
F1 "wice-address-s8.sch" 50
F2 "SEL" I L 4850 6700 60 
F3 "XCS1" I R 6050 6800 60 
F4 "RCS2" O L 4850 6800 60 
F5 "U6-A17" I L 4850 6900 60 
F6 "U8-A18" I L 4850 7000 60 
F7 "U6-XSEL" I L 4850 7100 60 
F8 "R19-A17" O L 4850 7200 60 
F9 "R16-A18" O L 4850 7300 60 
F10 "R18-SEL" O L 4850 7400 60 
F11 "U29E1" I L 4850 7600 60 
F12 "XA15" B R 6050 6900 60 
$EndSheet
Wire Wire Line
	6900 6800 6050 6800
Connection ~ 6900 4700
Wire Wire Line
	3900 6800 4850 6800
Connection ~ 3900 4500
Wire Wire Line
	8950 3750 8950 5700
Wire Wire Line
	8950 5700 6900 5700
Connection ~ 6900 5700
Wire Wire Line
	8400 2350 8400 5100
Wire Wire Line
	8400 5100 6800 5100
Connection ~ 6800 5100
Wire Wire Line
	6700 2450 6700 2150
Text Label 9200 3350 2    60   ~ 0
XOE
Text Notes 10050 4900 0    60   ~ 0
12
Text Notes 10200 4900 0    60   ~ 0
14
Text Notes 10050 4700 0    60   ~ 0
11
Text Notes 10050 4500 0    60   ~ 0
10
Text Notes 10100 4300 0    60   ~ 0
9
Text Notes 10100 4100 0    60   ~ 0
8
Text Notes 10100 3900 0    60   ~ 0
7
Text Notes 10100 3700 0    60   ~ 0
6
Text Notes 10100 3500 0    60   ~ 0
5
Text Notes 10100 3300 0    60   ~ 0
4
Text Notes 10100 3100 0    60   ~ 0
3
Text Notes 10100 2900 0    60   ~ 0
2
Text Notes 10100 2700 0    60   ~ 0
1
Text Notes 10050 4800 0    60   ~ 0
13
Text Notes 10050 4600 0    60   ~ 0
14
Text Notes 10050 4400 0    60   ~ 0
15
Text Notes 10050 4200 0    60   ~ 0
16
Text Notes 10050 4000 0    60   ~ 0
17
Text Notes 10050 3800 0    60   ~ 0
18
Text Notes 10050 3600 0    60   ~ 0
19
Text Notes 10050 3400 0    60   ~ 0
20
Text Notes 10050 3200 0    60   ~ 0
21
Text Notes 10050 3000 0    60   ~ 0
22
Text Notes 10050 2800 0    60   ~ 0
23
Text Notes 10050 2600 0    60   ~ 0
24
Text Notes 10200 4800 0    60   ~ 0
15
Text Notes 10200 4700 0    60   ~ 0
13
Text Notes 10200 4500 0    60   ~ 0
12
Text Notes 10200 4300 0    60   ~ 0
11
Text Notes 10200 4100 0    60   ~ 0
10
Text Notes 10250 3900 0    60   ~ 0
9
Text Notes 10250 3700 0    60   ~ 0
8
Text Notes 10250 3500 0    60   ~ 0
7
Text Notes 10250 3300 0    60   ~ 0
6
Text Notes 10250 3100 0    60   ~ 0
5
Text Notes 10250 2900 0    60   ~ 0
4
Text Notes 10250 2700 0    60   ~ 0
3
Text Notes 10250 2500 0    60   ~ 0
2
Text Notes 10250 2300 0    60   ~ 0
1
Text Notes 10200 4600 0    60   ~ 0
16
Text Notes 10200 4400 0    60   ~ 0
17
Text Notes 10200 4200 0    60   ~ 0
18
Text Notes 10200 4000 0    60   ~ 0
19
Text Notes 10200 3800 0    60   ~ 0
20
Text Notes 10200 3600 0    60   ~ 0
21
Text Notes 10200 3400 0    60   ~ 0
22
Text Notes 10200 3200 0    60   ~ 0
23
Text Notes 10200 3000 0    60   ~ 0
24
Text Notes 10200 2800 0    60   ~ 0
25
Text Notes 10200 2600 0    60   ~ 0
26
Text Notes 10200 2400 0    60   ~ 0
27
Text Notes 10200 2200 0    60   ~ 0
28
Text Notes 10350 4900 0    60   ~ 0
16
Text Notes 10350 4700 0    60   ~ 0
15
Text Notes 10350 4500 0    60   ~ 0
14
Text Notes 10350 4300 0    60   ~ 0
13
Text Notes 10350 4100 0    60   ~ 0
12
Text Notes 10350 3900 0    60   ~ 0
11
Text Notes 10350 3700 0    60   ~ 0
10
Text Notes 10400 3500 0    60   ~ 0
9
Text Notes 10400 3300 0    60   ~ 0
8
Text Notes 10400 3100 0    60   ~ 0
7
Text Notes 10400 2900 0    60   ~ 0
6
Text Notes 10400 2700 0    60   ~ 0
5
Text Notes 10400 2500 0    60   ~ 0
4
Text Notes 10400 2300 0    60   ~ 0
3
Text Notes 10400 2100 0    60   ~ 0
2
Text Notes 10400 1900 0    60   ~ 0
1
Text Notes 10350 4800 0    60   ~ 0
17
Text Notes 10350 4600 0    60   ~ 0
18
Text Notes 10350 4400 0    60   ~ 0
19
Text Notes 10350 4200 0    60   ~ 0
20
Text Notes 10350 4000 0    60   ~ 0
21
Text Notes 10350 3800 0    60   ~ 0
22
Text Notes 10350 3600 0    60   ~ 0
23
Text Notes 10350 3400 0    60   ~ 0
24
Text Notes 10350 3200 0    60   ~ 0
25
Text Notes 10350 3000 0    60   ~ 0
26
Text Notes 10350 2800 0    60   ~ 0
27
Text Notes 10350 2600 0    60   ~ 0
28
Text Notes 10350 2400 0    60   ~ 0
29
Text Notes 10350 2200 0    60   ~ 0
30
Text Notes 10350 2000 0    60   ~ 0
31
Text Notes 10350 1800 0    60   ~ 0
32
Text HLabel 1200 7000 0    60   Input ~ 0
U8-A18
Wire Wire Line
	4850 7000 1200 7000
Text HLabel 1200 7600 0    60   Input ~ 0
U29E1
Wire Wire Line
	4850 7600 1200 7600
Wire Wire Line
	6050 6900 6700 6900
Wire Wire Line
	6700 6900 6700 5400
Connection ~ 6700 5400
Wire Wire Line
	3900 2450 4850 2450
Wire Wire Line
	3800 2350 4850 2350
Wire Wire Line
	6900 3200 6900 4000
Wire Wire Line
	3700 3100 4850 3100
Wire Wire Line
	3900 3000 3900 4500
Wire Wire Line
	3600 4000 4850 4000
Wire Wire Line
	3700 3900 3700 4800
Wire Wire Line
	6900 4000 6900 4700
Wire Wire Line
	3700 4800 3700 5600
Wire Wire Line
	6800 4800 6800 5100
Wire Wire Line
	6900 4700 6900 5700
Wire Wire Line
	3900 4500 3900 6800
Wire Wire Line
	6900 5700 6900 6800
Wire Wire Line
	6800 5100 6800 5600
Wire Wire Line
	6700 5400 7400 5400
Wire Bus Line
	8000 6200 9500 6200
Text HLabel 9500 6200 2    60   BiDi ~ 0
XD[0..7]
$Comp
L power:GND #PWR?
U 1 1 5EBD407F
P 9400 5150
F 0 "#PWR?" H 9400 4900 50  0001 C CNN
F 1 "GND" H 9405 4977 50  0000 C CNN
F 2 "" H 9400 5150 50  0001 C CNN
F 3 "" H 9400 5150 50  0001 C CNN
	1    9400 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2150 9600 2150
Wire Wire Line
	7600 4050 9600 4050
Wire Wire Line
	1200 3600 4850 3600
Wire Wire Line
	1200 5200 4850 5200
Wire Bus Line
	8000 4050 8000 6200
Wire Bus Line
	2800 1250 2800 6200
Wire Bus Line
	7500 1250 7500 6200
$EndSCHEMATC
