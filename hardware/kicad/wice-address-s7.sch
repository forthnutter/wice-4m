EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 18 22
Title ""
Date "18 sep 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L wice-mod:74LS244-IEEE U17
U 1 1 59AFA096
P 5600 3400
F 0 "U17" H 5600 3850 60  0000 C CNN
F 1 "74LS244-IEEE" H 5600 3050 60  0000 C CNN
F 2 "~" H 5600 3400 60  0000 C CNN
F 3 "~" H 5600 3400 60  0000 C CNN
	1    5600 3400
	1    0    0    -1  
$EndComp
Text HLabel 3300 3100 0    60   Input ~ 0
SEL
Wire Wire Line
	5050 3100 3300 3100
Text HLabel 3300 3300 0    60   Input ~ 0
XA11
Text HLabel 3300 3400 0    60   Input ~ 0
XA12
Text HLabel 3300 3500 0    60   Input ~ 0
XA13
Wire Wire Line
	5050 3300 3300 3300
Wire Wire Line
	3300 3400 5050 3400
Wire Wire Line
	5050 3500 3300 3500
Wire Wire Line
	5050 3600 4700 3600
Wire Wire Line
	4700 3600 4700 4100
Text HLabel 7250 3300 2    60   Output ~ 0
RA11
Text HLabel 7250 3400 2    60   Output ~ 0
RA12
Text HLabel 7250 3500 2    60   Output ~ 0
RA13
Wire Wire Line
	6150 3300 7250 3300
Wire Wire Line
	7250 3400 6150 3400
Wire Wire Line
	6150 3500 7250 3500
$Comp
L power:GND #PWR?
U 1 1 5EC07588
P 4700 4100
F 0 "#PWR?" H 4700 3850 50  0001 C CNN
F 1 "GND" H 4705 3927 50  0000 C CNN
F 2 "" H 4700 4100 50  0001 C CNN
F 3 "" H 4700 4100 50  0001 C CNN
	1    4700 4100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
