EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 22
Title "Power Input"
Date "18 sep 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Diode:1N4148 D1
U 1 1 58677E53
P 4500 3400
F 0 "D1" H 4500 3500 40  0000 C CNN
F 1 "1N4148" H 4500 3300 40  0000 C CNN
F 2 "~" H 4500 3400 60  0000 C CNN
F 3 "~" H 4500 3400 60  0000 C CNN
	1    4500 3400
	-1   0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM7805_TO220 IC1
U 1 1 58677EBE
P 5400 3400
F 0 "IC1" H 5300 3700 60  0000 C CNN
F 1 "LM7805" H 5400 3600 60  0000 C CNN
F 2 "" H 5400 3400 60  0000 C CNN
F 3 "" H 5400 3400 60  0000 C CNN
	1    5400 3400
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4148 D2
U 1 1 58677ED6
P 6450 3400
F 0 "D2" H 6450 3500 40  0000 C CNN
F 1 "1N4148" H 6450 3300 40  0000 C CNN
F 2 "~" H 6450 3400 60  0000 C CNN
F 3 "~" H 6450 3400 60  0000 C CNN
	1    6450 3400
	-1   0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 58677F2C
P 4850 3800
F 0 "C1" H 4900 3900 40  0000 L CNN
F 1 "220uF 35V" H 4900 3700 40  0000 L CNN
F 2 "~" H 4950 3650 30  0000 C CNN
F 3 "~" H 4850 3800 300 0000 C CNN
	1    4850 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 58677F3B
P 7000 3800
F 0 "C2" H 7050 3900 40  0000 L CNN
F 1 "220uF 35V" H 7050 3700 40  0000 L CNN
F 2 "~" H 7100 3650 30  0000 C CNN
F 3 "~" H 7000 3800 300 0000 C CNN
	1    7000 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C32
U 1 1 58677F4A
P 7550 4450
F 0 "C32" H 7600 4550 40  0000 L CNN
F 1 "10uF 35V" H 7600 4350 40  0000 L CNN
F 2 "~" H 7650 4300 30  0000 C CNN
F 3 "~" H 7550 4450 300 0000 C CNN
	1    7550 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R32
U 1 1 58677F67
P 7550 3800
F 0 "R32" V 7630 3800 40  0000 C CNN
F 1 "10K" V 7557 3801 40  0000 C CNN
F 2 "~" V 7480 3800 30  0000 C CNN
F 3 "~" H 7550 3800 30  0000 C CNN
	1    7550 3800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Barrel_Jack_Switch PWR1
U 1 1 58678038
P 3700 3400
F 0 "PWR1" H 3700 3650 60  0000 C CNN
F 1 "BARREL_JACK" H 3700 3200 60  0000 C CNN
F 2 "" H 3700 3400 60  0000 C CNN
F 3 "" H 3700 3400 60  0000 C CNN
	1    3700 3400
	1    0    0    1   
$EndComp
Wire Wire Line
	4000 3400 4150 3400
$Comp
L power:GND #PWR?
U 1 1 58678107
P 4150 4350
AR Path="/58678107" Ref="#PWR?"  Part="1" 
AR Path="/58677DE2/58678107" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4150 4350 30  0001 C CNN
F 1 "GND" H 4150 4280 30  0001 C CNN
F 2 "" H 4150 4350 60  0000 C CNN
F 3 "" H 4150 4350 60  0000 C CNN
	1    4150 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3500 4150 3500
Wire Wire Line
	4150 3500 4150 4350
$Comp
L power:GND #PWR?
U 1 1 5867812A
P 4850 4350
AR Path="/5867812A" Ref="#PWR?"  Part="1" 
AR Path="/58677DE2/5867812A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4850 4350 30  0001 C CNN
F 1 "GND" H 4850 4280 30  0001 C CNN
F 2 "" H 4850 4350 60  0000 C CNN
F 3 "" H 4850 4350 60  0000 C CNN
	1    4850 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 3400 4850 3400
Wire Wire Line
	4850 3400 4850 3650
Wire Wire Line
	4850 3950 4850 4350
$Comp
L power:GND #PWR?
U 1 1 5867814D
P 5400 4350
AR Path="/5867814D" Ref="#PWR?"  Part="1" 
AR Path="/58677DE2/5867814D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5400 4350 30  0001 C CNN
F 1 "GND" H 5400 4280 30  0001 C CNN
F 2 "" H 5400 4350 60  0000 C CNN
F 3 "" H 5400 4350 60  0000 C CNN
	1    5400 4350
	1    0    0    -1  
$EndComp
Connection ~ 4850 3400
Wire Wire Line
	5400 3700 5400 4350
$Comp
L power:GND #PWR?
U 1 1 5867837C
P 7550 4800
AR Path="/5867837C" Ref="#PWR?"  Part="1" 
AR Path="/58677DE2/5867837C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7550 4800 30  0001 C CNN
F 1 "GND" H 7550 4730 30  0001 C CNN
F 2 "" H 7550 4800 60  0000 C CNN
F 3 "" H 7550 4800 60  0000 C CNN
	1    7550 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3400 6050 3400
Wire Wire Line
	6600 3400 6800 3400
Wire Wire Line
	7000 3400 7000 3650
$Comp
L power:GND #PWR?
U 1 1 586885E7
P 7000 4350
AR Path="/586885E7" Ref="#PWR?"  Part="1" 
AR Path="/58677DE2/586885E7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7000 4350 30  0001 C CNN
F 1 "GND" H 7000 4280 30  0001 C CNN
F 2 "" H 7000 4350 60  0000 C CNN
F 3 "" H 7000 4350 60  0000 C CNN
	1    7000 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3950 7000 4350
Wire Wire Line
	7550 3400 7550 3650
Connection ~ 7000 3400
Wire Wire Line
	7550 3950 7550 4150
Wire Wire Line
	7550 4600 7550 4800
Text HLabel 8550 4150 2    60   Output ~ 0
POR
Wire Wire Line
	8550 4150 7550 4150
Connection ~ 7550 4150
$Comp
L power:+5V #PWR?
U 1 1 58688664
P 7950 3350
F 0 "#PWR?" H 7950 3440 20  0001 C CNN
F 1 "+5V" H 7950 3500 30  0000 C CNN
F 2 "" H 7950 3350 60  0000 C CNN
F 3 "" H 7950 3350 60  0000 C CNN
	1    7950 3350
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 58688673
P 7650 3350
F 0 "#PWR?" H 7650 3450 30  0001 C CNN
F 1 "VCC" H 7650 3500 30  0000 C CNN
F 2 "" H 7650 3350 60  0000 C CNN
F 3 "" H 7650 3350 60  0000 C CNN
	1    7650 3350
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 58688682
P 7800 3350
F 0 "#PWR?" H 7800 3450 30  0001 C CNN
F 1 "VDD" H 7800 3500 30  0000 C CNN
F 2 "" H 7800 3350 60  0000 C CNN
F 3 "" H 7800 3350 60  0000 C CNN
	1    7800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 3400 7950 3350
Connection ~ 7550 3400
Wire Wire Line
	7800 3350 7800 3400
Connection ~ 7800 3400
Wire Wire Line
	7650 3350 7650 3400
Connection ~ 7650 3400
$Comp
L Device:Jumper LNK1
U 1 1 58688760
P 6450 3700
F 0 "LNK1" H 6450 3760 40  0000 C CNN
F 1 "LINK" H 6450 3630 40  0000 C CNN
F 2 "" H 6450 3700 60  0000 C CNN
F 3 "" H 6450 3700 60  0000 C CNN
F 4 "Use Link" H 6450 3700 60  0001 C CNN "Note"
	1    6450 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3700 6050 3700
Wire Wire Line
	6050 3700 6050 3400
Connection ~ 6050 3400
Wire Wire Line
	6650 3700 6800 3700
Wire Wire Line
	6800 3700 6800 3400
Connection ~ 6800 3400
Text Notes 2900 3000 0    60   ~ 0
7V to 9V DC Centre Negative
Wire Wire Line
	4000 3300 4150 3300
Wire Wire Line
	4150 3300 4150 3400
Connection ~ 4150 3400
Wire Wire Line
	4850 3400 5100 3400
Wire Wire Line
	7000 3400 7550 3400
Wire Wire Line
	7550 4150 7550 4300
Wire Wire Line
	7550 3400 7650 3400
Wire Wire Line
	7800 3400 7950 3400
Wire Wire Line
	7650 3400 7800 3400
Wire Wire Line
	6050 3400 6300 3400
Wire Wire Line
	6800 3400 7000 3400
Wire Wire Line
	4150 3400 4350 3400
$EndSCHEMATC
