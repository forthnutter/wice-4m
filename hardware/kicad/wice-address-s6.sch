EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 17 22
Title ""
Date "18 sep 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L wice-mod:74LS244-IEEE U20
U 1 1 59AF4C6E
P 5750 3200
F 0 "U20" H 6100 3600 60  0000 C CNN
F 1 "74LS244-IEEE" H 6000 2900 60  0000 C CNN
F 2 "~" H 5750 3200 60  0000 C CNN
F 3 "~" H 5750 3200 60  0000 C CNN
	1    5750 3200
	1    0    0    -1  
$EndComp
Text HLabel 3100 2900 0    60   Input ~ 0
SEL
Wire Wire Line
	5200 2900 3100 2900
Text HLabel 3100 3100 0    60   Input ~ 0
XA14
Text HLabel 3100 3200 0    60   Input ~ 0
XA15
Text HLabel 3100 3300 0    60   Input ~ 0
XA16
Text HLabel 3100 3400 0    60   Input ~ 0
XWE
Wire Wire Line
	3100 3100 5200 3100
Wire Wire Line
	5200 3200 3100 3200
Wire Wire Line
	3100 3300 5200 3300
Wire Wire Line
	5200 3400 3100 3400
Text HLabel 7350 3100 2    60   Output ~ 0
RA14
Text HLabel 7350 3200 2    60   Output ~ 0
RA15
Text HLabel 7350 3300 2    60   Output ~ 0
RA16
Text HLabel 7350 3400 2    60   Output ~ 0
RWE
Wire Wire Line
	6300 3100 7350 3100
Wire Wire Line
	7350 3200 6300 3200
Wire Wire Line
	6300 3300 7350 3300
Wire Wire Line
	7350 3400 6300 3400
$EndSCHEMATC
