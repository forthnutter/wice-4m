(c) Copyright 2014, Joseph Moschini ( forthnutter ), All Rights Reserved

==================================================
  Leap Electronics WICE-4M Reverse Engineering 
==================================================

Project folders:
pictures -- Photos of unit and PCB
hardware -- Schematic and PCB
* Datasheets
* designspark
* altium
software -- Old and the new


========================
       Background
========================

Leap Electronics WICE-4M is EPROM or ROM emulator.

Task 1
Trace and create a schematic of the PCB.

Task 2
Reverse Engineer software DOS or Windows.

Task 3
Create software to work on Linux OS.